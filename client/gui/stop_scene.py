#!/usr/bin/env python3

from typing import List, Union
import pyglet
import glooey
from glooey.containers import Stack
from glooey.images import Image

from client.gui.scene_information import SceneInformation, SceneType
from client.gui.scene import Scene
from client.gui.gui_elements import MessageLabel, CustomButton

class StopScene(Scene):
    """Initializes StopScene"""
    def __init__(self, scene_information: SceneInformation, window: 'Window', game: 'Game') -> None:
        """Initializes StopScene object.
        
        Args:
            scene_information(SceneInformation): provide a SceneInformation
            window (Window):
            game (Game):
            
        Raises:
            TypeError: StopScene object can only be initialized with scene information of type STOP_SCENE"""
        if scene_information.scene_type != SceneType.STOP_SCENE:
            raise TypeError("Provided scene information must be of type SceneType.STOP_SCENE but is of type %s" 
                            % scene_information.scene_type)
        super().__init__(scene_information, window, game)


    def create(self) -> None:
        super().create()
        # create stack that contains all widgets
        self.__stack = Stack()
        self._gui.add(self.__stack)

        # add background image
        self.__background = Image(image=pyglet.resource.image('background.png'))
        self.__stack.add(self.__background)
        
        # create message label
        message_text = self.__create_label_text(self._scene_information.winner_ids, self._scene_information.winner_lives)
        self.__label = MessageLabel(message_text, line_wrap=True)
        line_wrap_width = int(self._window.get_size()[0] * 0.8)
        self.__label.enable_line_wrap(line_wrap_width)
        self.__label.set_alignment('bottom')
        self.__label.set_padding(bottom=100)
        self.__stack.add(self.__label)

        self.__button = CustomButton("Return")
        self.__button.push_handlers(on_click=lambda w: self._game.wait_for_other_players())
        self.__button.set_alignment('bottom')
        self.__button.set_padding(bottom=20)
        self.__stack.add(self.__button)


    def _modify(self):
        pass
    
    @staticmethod
    def __create_label_text(winner_ids: List[int], winner_lives: Union[int, None]) -> str:
        """
        Creates label text about winners, depending on the number of them.

        Args:
            winner_ids (List[int]): Winners' identifiers
            winner_lives (Union[int, None]): Winners' number of remaining lives

        Returns:
            str: Label text
        """
        
        if winner_lives is None:
            return "No one has won!"
        
        winners_str = []
        for idx, winner_id in enumerate(winner_ids):
            if idx == 0:
                seperator = ""
            elif idx == len(winner_ids) - 1:
                seperator = " and "
            else:
                seperator = ", "
            winners_str.append(seperator)
            winner_str = "Player %d" % winner_id
            winners_str.append(winner_str)
        
        verb_str = "has" if len(winner_ids) == 1 else "have"
        life_str = "life" if winner_lives == 1 else "lives"
        
        return "%s %s won with %d remaining %s!" % ("".join(winners_str), verb_str, winner_lives, life_str)
