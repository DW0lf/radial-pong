#!/usr/bin/env python3

import pyglet
from glooey.containers import Stack
from glooey.images import Image

from client.gui.scene_information import SceneInformation, SceneType
from client.gui.scene import Scene
from client.gui.gui_elements import CustomButton, MessageLabel

class WaitingScene(Scene):
    """Implements WaitingScene"""


    def __init__(self, scene_information: SceneInformation, window: 'Window', game: 'Game') -> None:
        """Initializes WaitingScene object.
        
        Args:
            scene_information(SceneInformation): provide a SceneInformation
            window (Window): window instance
            game (Game): game instance
            
        Raises:
            TypeError: WaitingScene object can only be initialized with scene information of type WAITING_SCENE"""
        if scene_information.scene_type != SceneType.WAITING_SCENE:
            raise TypeError("Provided scene information must be of type SceneType.WAITING_SCENE but is of type %s" % scene_information.scene_type)
        super().__init__(scene_information, window, game)


    def create(self) -> None:
        super().create()
        # create stack that contains all widgets
        self.__stack = Stack()
        self._gui.add(self.__stack)

        # add background image
        self.__background = Image(image=pyglet.resource.image('background.png'))
        self.__stack.add(self.__background)

        # create groups
        self.__background = pyglet.graphics.OrderedGroup(0)
        self.__foreground = pyglet.graphics.OrderedGroup(1)
        
        # create loading images
        x_pos = self._window_scale[0] // 2
        y_pos = 375
        platform_image = pyglet.resource.image('icon_platforms.png')
        platform_image.anchor_x = platform_image.width // 2
        platform_image.anchor_y = platform_image.height // 2 - 20
        self.__loading_sprite = pyglet.sprite.Sprite(platform_image,
                                                    x=x_pos,
                                                    y=y_pos,
                                                    batch=self._graphics_foreground_batch,
                                                    group=self.__foreground)
        self.__loading_sprite.scale = 0.5
        ball_image = pyglet.resource.image('icon_ball.png')
        ball_image.anchor_x = ball_image.width // 2
        ball_image.anchor_y = ball_image.height // 2
        self.__loading_sprite_static = pyglet.sprite.Sprite(ball_image,
                                                    x=x_pos,
                                                    y=y_pos,
                                                    batch=self._graphics_foreground_batch,
                                                    group=self.__background)
        self.__loading_sprite_static.scale = 0.5

        # set animation parameters
        self.__rotation_angle = 0
        self.__rotation_speed = 10
        self.__rotation_shadow_offset = 3
        
        # create player count label
        self.__player_count_label = MessageLabel(text="Connected players: %d" % self._game.player_count)
        self.__player_count_label.set_alignment('top')
        self.__player_count_label.set_padding(top=50)
        self.__stack.add(self.__player_count_label)

        # create waiting label
        self.__waiting_label = MessageLabel(text="Waiting for players ...")
        self.__waiting_label.set_alignment('bottom')
        self.__waiting_label.set_padding(bottom=100)
        self.__stack.add(self.__waiting_label)

        # create ready button
        self.__start = CustomButton("Start")
        self.__start.set_alignment('bottom')
        self.__start.set_padding(bottom=50)
        self.__start.push_handlers(on_click=self._player_start)
        self.__stack.add(self.__start)

        # get pyglet clock
        self.__clock = pyglet.clock
        self.__clock.schedule(self.__draw_animation)


    def _modify(self) -> None:
        """Updates number of participating players."""
        
        self.__player_count_label.set_text("Connected players: %d" % self._game.player_count)

    def __draw_animation(self, dt : float) -> None:
        """Draw the loading icon animation with pyglet clock.
        
        Args:
            dt(float): time passed between frames
        """
        self.__loading_sprite.update(rotation=self.__rotation_angle)
        self.__rotation_angle += self.__rotation_speed * dt

    def _player_start(self, widget : 'CustomButton'):
        """Method run by \"Ready\" button, to request game start.
        
        Args:
            widget(CustomButton): button widget to act as a handler function
        """
        self._game.request_game_start()
