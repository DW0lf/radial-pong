#!/usr/bin/env python3

"""Implements a customized event loop"""

import pyglet

class EventLoop(pyglet.app.EventLoop):
    """Implements a customized event loop"""
    def __init__(self, window: 'Window') -> None:
        """Initialized customized event loop.

        Args:
            window (Window): current window
        """
        super().__init__()
        self.__window = window
    
    def on_window_close(self, window: pyglet.window.Window) -> None:
        window.on_close()
