#!/usr/bin/env python3

"""Customized text edit and button matching the desired design.
CustomButton implements the concrete button. CustomEdit implements the concrete 
text edit. The other classes are just used internally.
"""

import clipboard
import glooey
import pyglet
from pyglet.window import key
from typing import Tuple
from functools import partial

# specify font style
FONT_COLOR = "#202020"
FONT_SIZE = 12
FONT_RED_COLOR = "#e81313"

# specify paths of background images for button and label
BASE_IMAGE = pyglet.image.load("./radialpong/client/gui/img/base.png")
TEXT_IMAGE = pyglet.image.load("./radialpong/client/gui/img/text.png")

class ErrorLabel(glooey.Label):
    """Implements a customized label, with specific font settings, but without 
    a background. This label is used for error messages.
    
    Usage:
        label = ErrorLabel("Text")
    """
    custom_font_size = FONT_SIZE
    custom_color = FONT_RED_COLOR
    custom_alignment = 'center'
    custom_bold = True 
    custom_padding = 4
    
class CopyrightLabel(glooey.Label):
    """Implements a customized label, with specific font settings, but without 
    a background. This label is used for copyright information.
    
    Usage:
        label = CopyrightLabel("Text")
    """
    custom_font_size = FONT_SIZE - 2
    custom_color = FONT_COLOR
    custom_alignment = 'center'
    custom_padding = 4
    custom_line_spacing = custom_font_size * 2
    custom_text_alignment = 'right'

class MessageLabel(glooey.Label):
    """Implements a customized label, with specific font settings, but without 
    a background. This label is used for most messages.
    
    Usage:
        label = MessageLabel("Text")
    """
    custom_font_size = FONT_SIZE
    custom_color = FONT_COLOR
    custom_alignment = 'center'
    custom_bold = True 
    custom_padding = 4

class CustomImageButton(glooey.Button):
    """Implements a customized button with just images."""

    def __init__(self, base_image, over_image):
        super().__init__()
        self.set_base_background(glooey.images.Image(base_image))
        self.set_over_background(glooey.images.Image(over_image))
        self.set_down_background(glooey.images.Image(over_image))

class EditLabel(glooey.EditableLabel):
    """Implements a customized, editable label, with specific font settings, but 
    without a background. This editable label is used as part of an styled edit 
    field. On initialization an initial content for the field can be determined. 
    If a mouse press inside of this field is recognized and it contains the 
    initial text, the text is deleted from it. If the user enters nothing into 
    the field, the initial text is inserted.
    
    Usage:
        label = EditLabel(text) # text is the initial content
    """
    custom_font_size = 11
    custom_alignment = "center"
    custom_top_padding = 13
    custom_color = FONT_COLOR
    custom_selection_background_color = "#00DD00"
    custom_selection_color = "#FFFFFF"

    def __init__(self, base_size: Tuple[int, int], text: str, line_wrap: int = 0, 
    **style) -> None:
        """Initialize customized button with a specific size.
        The returned Label has a width which is by 10px smaller than the first 
        the element in base_size, which determines the width of background. The 
        height is equal to the second element of base_size.
        
        Args:
            base_size(tuple(int, int)): tuple of width and height 
                specifying the minimal size of the background
            line_wrap(int): refer to glooey documentation (default: 0)
            **style: refer to glooey documentation
        """
        self.__initial_text = text
        self.custom_size_hint = (base_size[0]-10, base_size[1])
        super().__init__(text=text, line_wrap=line_wrap, **style)
    
    def on_mouse_press(self, x:int, y:int, button:int, modifiers:int) -> None:
        """Implements the behaviour on mouse click into the editable field. If 
        the initial text is still set, than the text is removed on mouse press.
        
        Args:
            x(int): x pixel coordinate of click position
            y(int): y pixel coordinate of click position
            button(int): type of mouse button (forwarded to method of super class)
            modifiers(int): modifiers for mouse press event (forwarded to 
                method of super class)
        """
        if self.get_text() == self.__initial_text:
            self.set_text("")
        super().on_mouse_press(x, y, button, modifiers)
    
    def on_window_key_press(self, symbol: int, modifiers: int) -> bool:
        """
        Is invoked on any key press, while edit label is active. Provides QoL features like copy/paste/cut for 
        EditLabels.

        Args:
            symbol (int): Pressed key
            modifiers (int): Pressed modifier keys
            
        Returns:
            bool: Key press event was successfully triggered
        """
        
        def get_selection_slice() -> slice:
            """
            Returns slice object that represents text selection.

            Returns:
                slice: Slice object representing text selection
            """
            return slice(self._layout.selection_start, self._layout.selection_end)
        
        if symbol == key.C and modifiers & key.MOD_CTRL:
            selection = get_selection_slice()
            clipboard.copy(self.get_text()[selection])
            self.unfocus()
        if symbol == key.X and modifiers & key.MOD_CTRL:
            selection = get_selection_slice()
            current_text = self.get_text()
            clipboard.copy(current_text[selection])
            new_text = current_text[0:selection.start] + current_text[selection.stop:]
            self.set_text(new_text)
            self._layout.set_selection(0, 0)
            self.unfocus()
        elif symbol == key.V and modifiers & key.MOD_CTRL:
            current_text = self.get_text()
            pasted_text = clipboard.paste()
            
            selection = get_selection_slice()
            selection_length = selection.stop - selection.start
            if selection_length > 0:
                new_text = current_text[0:selection.start] + pasted_text + current_text[selection.stop:]
            else:
                new_text = current_text[:self._caret.position] + pasted_text + current_text[self._caret.position:]
            self.set_text(new_text)
            self._caret._set_position(self._caret.position + len(pasted_text))
            self.unfocus()
            
        return super().on_window_key_press(symbol, modifiers)
    
    def unfocus(self) -> None:
        """Implements the behaviour on unfocus (cursor is removed from the field). 
        If the field is unfocused and it does not contain anything, the initial 
        text is inserted.
        """
        if self.get_text() == "":
            self.set_text(self.__initial_text)
        super().unfocus()


class SmallBase(glooey.Background):
    """Implements a small field as background for the CustomEdit. This field 
    holds the specified image as background."""
    custom_image = BASE_IMAGE
    custom_alignment = 'center'
    

class LargeBase(glooey.Background):
    """Implements a large field as background for the CustomEdit. This field 
    holds the specified image as background."""
    custom_image = TEXT_IMAGE
    custom_alignment = 'center'


class CustomEdit(glooey.Form):
    """Implements a customized edit field, which matches the desired design."""
    LARGE = 0
    SMALL = 1

    def __init__(self, text:str, size:int=0) -> None:
        """Initialize the customized edit field.
        
        Args:
            text(str): intial text to be shown in the edit
            size(str): integer specifying the size (CustomEdit.LARGE = 0 and 
                CustomEdit.SMALL = 1 are allowed values; default is 0)
        
        Raises:
            ValueError: invalid value for argument size
        """
        if size == CustomEdit.SMALL:
            self.Base = SmallBase
        elif size == CustomEdit.LARGE:
            self.Base = LargeBase
        else:
            raise ValueError("Invalid Value for size argument.")
        self.custom_alignment = "center"
        self.Label = partial(EditLabel, (self.Base.custom_image.width, 
            self.Base.custom_image.height), text)
        super().__init__()
