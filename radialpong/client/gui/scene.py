#!/usr/bin/env python3

from typing import Any
import pyglet
import glooey


class Scene:
    
    # colors of the players (their platforms and labels; red, gree, blue, magenta, dark green, violet, orange, black)
    _PLAYER_COLORS = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 0, 255), (0, 128, 0), (128, 0, 128), 
                                (255, 128, 0), (0, 0, 0)]
    
    """Abstract Scene class"""
    def __init__(self, scene_information : 'SceneInformation', window: 'Window', game: 'Game', resources: 'pyglet.resource.Loader') -> None:
        """Initializes Scene.
        Args:
            scene_information(SceneInformation): 
            window (Window):
            game (Game):
            resources(pyglet.resource.Loader): central resource loader
            
        Raises:
            NotImplementedError: Must be overridden for concrete scene classes"""
        if type(self) is Scene:
            raise NotImplementedError
        self._scene_information = scene_information
        self._window = window
        self._resources = resources
        self._game = game
        self._sprites = list()
    
    def __setattr__(self, name: str, value: Any) -> None:
        """
        Adds all instance assigned sprites to a list in order to delete on scene change.

        Args:
            name (str): Name of the assigned attribute
            value (Any): Assigned attribute
        """
        
        if isinstance(value, pyglet.sprite.Sprite):
            self._sprites.append(value)
        super().__setattr__(name, value)
        
    @property
    def scene_type(self) -> 'SceneType':
        """Getter for scene_type.
        
        Returns:
            SceneType: Scene type of SceneInformation attribute
        """
        return self._scene_information.scene_type
    
    def create(self) -> None:
        """Creates all GUI Elements"""
        self._gui_batch = pyglet.graphics.Batch()
        self._graphics_batch = pyglet.graphics.Batch()
        self._graphics_foreground_batch = pyglet.graphics.Batch()
        self._gui = glooey.Gui(self._window, batch=self._gui_batch, clear_before_draw=False)
        self._window_scale = self._window.get_size()
    
    def clear(self) -> None:
        """Clears the scene and removes all of its GUI elements."""
        
        for sprite in self._sprites:
            sprite.delete()
        self._gui.clear()

    def update(self, scene_information : 'SceneInformation') -> None:
        """Update scene information with appropiate new scene information.
        
        Args:
            scene_information(SceneInformation): new scene information
        
        Raises:
            TypeError: If a scene information has the wrong scene type
        """
        
        if self._scene_information.scene_type != scene_information.scene_type:
            raise TypeError("Given scene information is not of type %s" % self._scene_information.scene_type)

        self._scene_information = scene_information
        self._modify()

    def _modify(self) -> None:
        """Is called when the scene_information changes."""
        raise NotImplementedError

    def draw(self) -> None:
        """Draws the scene."""
        self._graphics_batch.draw()
        self._gui.on_draw()
        self._graphics_foreground_batch.draw()
