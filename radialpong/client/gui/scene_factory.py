#!/usr/bin/env python3

"""Implements a factory for different types of Scene."""

from client.gui.scene_information import SceneInformation, SceneType
from client.gui.start_scene import StartScene
from client.gui.waiting_scene import WaitingScene
from client.gui.game_scene import GameScene
from client.gui.stop_scene import StopScene

def create_scene(scene_information : 'SceneInformation', window: 'Window', game: 'Game', resources: 'pyglet.resource.Loader') -> 'Scene':
    """Creates a specific scene type depending on the concrete type of scene_information.

    Args:
        scene_information(SceneInformation): object containing the information to create the scene from; the return type of this method depends on the conrete type of this parameter
        window(Window): window object for plotting
        game(Game): object representing the current game
        resources(pyglet.resource.Loader): central resource loader
    
    Returns:
        StartScene: scene_information is of type StartSceneInformation
        WaitingScene: scene_information is of type WaitingSceneInformation
        GameScene: scene_information is of type GameSceneInformation
        StopScene: scene_information is of type StopSceneInformation
    
    Raises:
        TypeError: scene_information is not one of the aforementioned type
    """
    scene = None
    if scene_information.scene_type == SceneType.START_SCENE:
        scene = StartScene(scene_information, window, game, resources)
    elif scene_information.scene_type == SceneType.WAITING_SCENE:
        scene = WaitingScene(scene_information, window, game, resources)
    elif scene_information.scene_type == SceneType.GAME_SCENE:
        scene = GameScene(scene_information, window, game, resources)
    elif scene_information.scene_type == SceneType.STOP_SCENE:
        scene = StopScene(scene_information, window, game, resources)
    else:
        raise TypeError("Unsupported scene type provided!")
    return scene
