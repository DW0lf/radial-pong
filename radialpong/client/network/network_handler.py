#!/usr/bin/env python3

"""Implements the network communication layer on the client side."""


import time
import sys
import os
sys.path.append(os.getcwd())
import socket
from typing import Tuple

from queue import Queue
from shared.network.connection_handler import BaseConnectionHandler
from shared.network.network_handler import BaseNetworkHandler


CLIENT_SLEEP = 1e-3

class NetworkConfiguration():
    """Datacontainer for IP address and network port of server application."""
    def __init__(self, ip_addr: str, port: int) -> None:
        """Initialize data container for network configuration.
        
        Args:
            ip_addr(str): ip address of server
            port(int): port of server application
        """
        self._ip_addr = ip_addr
        self._port = port
    
    @property
    def ip_addr(self) -> str:
        """Getter for ip address of server to connect to.

        Returns:
            str: ip address of server
        """
        return self._ip_addr
    
    @property
    def port(self) -> int:
        """Getter for port of server to connect to.

        Returns:
            int: port of server
        """
        return self._port
    
    def get_addr_tuple(self) -> Tuple[str, int]:
        """Returns a tuple of ip adress and port of game server, which is suitable 
        for calls in the socket library.

        Returns:
            (str, int): tuple of ip address and port
        """
        return (self._ip_addr, self._port) 


class ClientNetworkHandler(BaseNetworkHandler):
    """Specializes the BaseNetworkHandler for the client side."""
    def __init__(self, serializer: 'Serializer', network_config: NetworkConfiguration) -> None:
        """Initializes super-class, create client-specific attributes and start listener thread as well as handler thread.

        Args:
            serializer (Serializer): current Serializer instance
            network_config (NetworkConfiguration): container for connection information
        
        Raises:
            ConnectionRefusedError: connection to server failed
        """
        super().__init__(serializer)
        
        # client-specific attributes
        self.__network_config = network_config
        self.__connection_handler = None

        # connect to server
        connection_status = self.__connect_server()

        # raise error if connection failed
        if not connection_status:
            self._close()
            raise ConnectionRefusedError("Could not connect to server with "
                "address %s:%d." % (network_config.ip_addr, network_config.port))

        # set up and start listener
        self._start_listener()

        # start socket handler in separate thread
        self._start_handler()  

    def __connect_server(self) -> bool:
        """Starts connection to specified server socket.

        Returns:
            Boolean: boolean specifying status of connection (True: connection successful, 
                False: connection failed)
        """
        server_addr = self.__network_config.get_addr_tuple()
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self._sock.connect(server_addr)
        except ConnectionRefusedError:
            # connection failed
            return False
        
        # connection successful
        self._send_buffers.append(Queue()) 
        self._recv_buffers.append(Queue())
        self._sock.setblocking(False)
        self.selector.register(self._sock, ClientNetworkHandler.READ_WRITE_EVENT, data=None)
        self.__connection_handler = BaseConnectionHandler(self.selector, self._send_buffers[0], self._recv_buffers[0], self.condition)

        self._running = True
        print("Connected to server")
        return True

    def run(self) -> None:
        """Overwrites BaseNetworkHandler's run method. Executes sending and receiving data. This method should be run in 
        a separate thread.
        """
        while self._running:
            events = self.selector.select(timeout=None)
            time.sleep(CLIENT_SLEEP)    # this is necessary due to selectors' select 
            # function returning immediately, because the sockets are always 
            # ready to write and thereby this function would poll
            for key, mask in events:
                self.__connection_handler.handle(key, mask)
            
            # Check for a socket being monitored to continue.
            if not self.selector.get_map():
                # leave loop if socket is closed
                break
    
    def send(self, content : bytes) -> None:
        """Overwrite BaseNetworkHandler's send method to send the message to the server. Puts serialized message into sending queue.

        Args:
            content(bytes): Data to send
        """
        self._send_message(content, 0)
        
    def __block_until_sendq_empty(self, timeout: int, freq: int=1) -> None:
        """
        Blocks until send queue is empty. Polls at given frequency, max. until timeout is reached.
        
        Args:
            timeout (int): Timeout for blocking, in seconds
            freq (int): Polling frequency (optional)
        """
        # check if connection was already started
        if self._send_buffers: 
            max_poll = freq * timeout
            sleep_time = 1. / freq
            for _ in range(max_poll):
                if self._send_buffers[0].empty():
                    break
                time.sleep(sleep_time)

    def close_connection(self, timeout: int=15) -> None:
        """Specializes BaseNetworkHandler's close_connection to wait for messages to be send until timeout.
        
        Args:
            timeout(int): Timeout until connection is eventually closed (optional)
        """
        # block until send queue is empty
        self.__block_until_sendq_empty(timeout)
        self._running = False
        super().close_connection()
