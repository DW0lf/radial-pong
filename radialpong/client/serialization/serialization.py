#!/usr/bin/env python3

from shared.serialization.client_disconnect_message import ClientDisconnectMessage
from shared.serialization.serializable_message_type import SerializableMessageType
from shared.serialization.base_serializer import BaseSerializer
from client.network.network_handler import NetworkConfiguration, ClientNetworkHandler


class Serializer(BaseSerializer):
    """Implements interface for serializer in client enviroments."""
    
    def __init__(self, game: 'Game', network_config: NetworkConfiguration) -> None:
        """
        Initializes Serializer.
        
        Args:
            game (client.game.game.Game): Client-side game instance
            network_config (NetworkConfiguration): Network configuration containing ip address and host
        
        Raises:
            ConnectionRefusedError: creation of ClientNetworkHandler fails due to failed 
                connection to the server
        """
        
        super().__init__()
        self.game = game
        
        # the initialization of ClientNetworkHandler may raise an ConnectionRefusedError 
        # which is not catched here, but is catched in Game (where Serializer is 
        # instanced) to provide a status on connection request
        self.network_handler = ClientNetworkHandler(self, network_config)
    
    @property
    def network_handler(self) -> ClientNetworkHandler:
        """
        Getter-method for ClientNetworkHandler instance.

        Returns:
            ClientNetworkHandler: network handler instance, managing connection to server
        """
        
        return self.__network_handler
    
    @network_handler.setter
    def network_handler(self, network_handler: ClientNetworkHandler) -> None:
        """Setter-method for ClientNetworkHandler instance.

        Args:
            network_handler (ClientNetworkHandler): network handler instance, managing connection to server
        """
        
        self.__network_handler = network_handler
    
    @property
    def game(self) -> 'Game':
        """
        Getter-method for Game instance.

        Returns:
            Game: Game instance, implementing game representation
        """
        
        return self.__game
    
    @game.setter
    def game(self, game: 'Game') -> None:
        """Setter-method for Game instance.

        Args:
            game (Game): Game instance, implementing game representation
        """
        
        self.__game = game
    
    def deserialize(self, content_data: bytes, player_id:int = 0) -> None:
        """
        Deserializes data according to the included message type and updates game.

        Args:
            content_data (bytes): Data to be deserialized
        """
        
        # get message data
        message = self._get_message(content_data)
        
        # call updating method of game layer
        self.__game.update(message)
        
    
    def serialize(self, message: SerializableMessageType) -> None:
        """
        Serializes data according to the given message type and sends to server.

        Args:
            message (SerializableMessageType): Message to be serialized and sent
        """
        
        # get content bytes
        content_bytes = self._get_content_bytes(message)
        
        # call sending method of network layer
        self.__network_handler.send(content_bytes)
        
    def close(self, timeout=15) -> None:
        """Initiates network connection closing.
        
        Args:
            timeout(int): Timeout until connection is eventually closed (optional)
        """
        
        # create ClientDisconnectMessage and send to server
        client_disconnect_message = ClientDisconnectMessage()
        self.serialize(client_disconnect_message)
        
        # initiate socket closing, wait 15 seconds until message has to be sent
        self.__network_handler.close_connection(timeout)
