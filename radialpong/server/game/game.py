#!/usr/bin/env python3

"""Script implements the game class."""

import math
from typing import List, Tuple, Union
from shared.serialization.is_alive_message import IsAliveMessage
import threading
import time
import sys

from server.serialization.serialization import Serializer
from shared.game.game_components import GameState, Ball
from shared.serialization.serializable_message_type import SerializableMessageType
from shared.serialization.connection_request_message import ConnectionRequestMessage
from shared.serialization.connection_confirmation_message import ConnectionConfirmationMessage, ConnectionStatus
from shared.serialization.player_count_message import PlayerCountMessage
from shared.serialization.game_start_request_message import GameStartRequestMessage
from shared.serialization.game_start_message import GameStartMessage
from shared.serialization.game_stop_message import GameStopMessage
from shared.serialization.game_state_message import GameStateMessage
from shared.serialization.client_input_message import ClientInputMessage
from shared.serialization.client_disconnect_message import ClientDisconnectMessage
import shared.game.math_util as math_util

SECONDS_TO_NANOSECONDS = 1e9

class Game:
    """Controlls the game.

    Attributes:
        __game_state (GameState): The current game state. None if the game is not running.
        __connected_players (List[int]): Player ids connected to the game.
        __game_is_running (bool): Flag if game is running.
        __serializer(Serializer): serializer of server, which serializes messages 
            and passes them to the ServerNetworkHandler
    
    Usage: 
        1. Instance Game.
        2. Call start_server method of created object.
        3. Now the Game is ready to receive and process messages from the 
        intialized Serializer.
    """

    MAX_TICK_SPEED = 1/30 # 30 ticks per second

    def __init__(self):
        self.__game_state = None
        self.__connected_players = []
        self.__game_is_running = False
        self.__serializer = None

    def start_server(self) -> None:
        """Start the Serializer and ServerNetworkHandler. Stops the program if binding 
        of socket fails.
        """
        try:
            self.serializer = Serializer(self)
        except ConnectionError as error:
            print(error)
            sys.exit([0])

    @property
    def serializer(self) -> 'Serializer':
        """
        Getter-method for attribute \"serializer\".

        Returns:
            Serializer: Serializer to communicate with the network.
        """
        return self.__serializer
    
    @serializer.setter
    def serializer(self, serializer: 'Serializer') -> None:
        """
        Setter-method for attribute \"remaining_lives\".

        Args:
            serializer (Serializer): Serializer to communicate with the network.
        """
        self.__serializer = serializer

    def update(self, message: SerializableMessageType):
        """Updates the game with the informations of a given message.

        Args:
            message (SerializableMessageType): Received message
        """
        if isinstance(message, ConnectionRequestMessage):
            player_id = message.player_id
            self.__add_player(player_id)
            connection_confirmation_message = self.__create_connection_confirmation_message(player_id)
            self.serializer.serialize_for_one(connection_confirmation_message, player_id)
            player_count_message = self.__create_player_count_message()
            self.serializer.serialize_for_all(player_count_message)
        elif isinstance(message, ClientDisconnectMessage):
            self.__remove_player(message.player_id)
            if len(self.__connected_players) > 0:
                player_count_message = self.__create_player_count_message()
                self.serializer.serialize_for_all(player_count_message)
        elif isinstance(message, GameStartRequestMessage):
            if self.__ready_to_start():
                self.__set_up()
                game_start_message = self.__create_game_start_message()
                self.serializer.serialize_for_all(game_start_message)
                self.__start_game_loop()
        elif isinstance(message, ClientInputMessage):
            player_id = message.player_id
            message_angle = message.player_angle
            self.__update_player_input(player_id, message_angle)
        elif isinstance(message, IsAliveMessage):
            self.serializer.serialize_for_one(IsAliveMessage(), message.player_id)

    def __create_connection_confirmation_message(self, player_id: int) -> ConnectionConfirmationMessage:
        """Creates a connection confirmation message.

        Args:
            player_id (int): Id of the receiving player.

        Returns:
            ConnectionConfirmationMessage: Created message
        """
        connection_status = ConnectionStatus.SUCCESSFUL_WAITING
        if self.__game_is_running:
            connection_status = ConnectionStatus.SUCCESSFUL_RUNNING
        message = ConnectionConfirmationMessage(player_id, connection_status)
        return message
    
    def __create_player_count_message(self) -> PlayerCountMessage:
        """Creates a player count message.

        Returns:
            PlayerCountMessage: Created message
        """
        
        player_count = len(self.__connected_players)
        return PlayerCountMessage(player_count)

    def __create_game_start_message(self) -> GameStartMessage:
        """creates a game start message.

        Returns:
            GameStartMessage: Created message
        """
        return GameStartMessage(self.__connected_players)

    def __create_game_stop_message(self, winner_properties: Tuple[list, Union[int, None]]) -> GameStopMessage:
        """Creates a game stop message.
        
        Args:
            winner_properties (Tuple[list, Union[int, None]): Ids and remaining lives of the winners

        Returns:
            GameStopMessage: Created message
        """
        return GameStopMessage(*winner_properties)

    def __create_game_state_message(self) -> GameStateMessage:
        """Creates a game state message.

        Returns:
            GameStopMessage: Created message
        """
        ball_positions = []
        for ball in self.__game_state.balls:
            ball_positions.append((ball.position[0], ball.position[1]))
        player_angles = []
        player_lifes = []
        number_of_players = self.__game_state.number_of_players
        for i in range(number_of_players):
            player = self.__game_state.players[i]
            message_angle = math_util.get_message_angle_from_angle(number_of_players, player.platform.angle)
            player_angles.append(message_angle)
            player_lifes.append(player.remaining_lives)
        return GameStateMessage(ball_positions, player_angles, player_lifes)

    def __add_player(self, player_id: int) -> None:
        """Adds a new player to the game.

        Args:
            player_id (int): Id of the new player.
        """
        self.__connected_players.append(player_id)
        print("Player joined game")

    def __remove_player(self, player_id: int) -> None:
        """Removes a player from the game.

        Args:
            id (int): Id of the player to remove.
        """
        if player_id in self.__connected_players:
            self.__connected_players.remove(player_id)
            print("Player left game")

    def __update_player_input(self, player_id: int, message_angle: float) -> None:
        """Updates the angle of a player.
        
        Args:
            player_id (int): Id of the player
            message_angle (float): New angle of the players platform between 0 and 1
        """
        number_of_players = len(self.__game_state.players)
        for i in range(number_of_players):
            player = self.__game_state.players[i]
            if player.player_id == player_id:
                player.platform.angle = math_util.get_angle_from_message_angle(number_of_players, i, message_angle)
                break

    def __ready_to_start(self) -> bool:
        """Checks if the game is ready to start

        Returns:
            bool: Game is ready
        """
        return not self.__game_is_running and len(self.__connected_players) > 1

    def __set_up(self) -> None:
        """Creates the gamestate before start."""
        number_of_players = len(self.__connected_players)
        self.__game_state = GameState(number_of_players)
        for i in range(number_of_players):
            player_id = self.__connected_players[i]
            self.__game_state.add_player(player_id)
        
        for i in range(number_of_players):
            angle = math_util.get_angle_from_message_angle(number_of_players, i, 0.5)
            velocity_vector = math_util.get_cartesian_coordinate([0, 0], 1, angle)
            ball = Ball(self.__game_state.CENTER, velocity_vector)
            self.__game_state.balls.append(ball)

    def __start_game_loop(self) -> None:
        """Starts the game loop."""
        self.__game_is_running = True
        game_loop = threading.Thread(target=self.__game_loop)
        game_loop.start()
        print("Game started")

    def __stop(self) -> None:
        """Stops the game."""
        self.__game_is_running = False
        winner_properties = self.__get_winner_properties()
        game_stop_message = self.__create_game_stop_message(winner_properties)
        self.serializer.serialize_for_all(game_stop_message)
        print("Game stopped")
        self.__serializer.print_statistics()

    def __game_loop(self) -> None:
        """Loop that runs while the game is runnning.

        Note:
            Should be run in a seperate thread.
        """
        last_time = time.time_ns()
        while self.__game_is_running:
            time_till_next_tick = Game.MAX_TICK_SPEED - (time.time_ns() - last_time) / SECONDS_TO_NANOSECONDS
            if time_till_next_tick > 0:
                time.sleep(time_till_next_tick)
            current_time = time.time_ns()
            delta_time = (current_time - last_time) / SECONDS_TO_NANOSECONDS #Delta time in seconds
            last_time = current_time

            self.__update_gamestate(delta_time)
            game_state_message = self.__create_game_state_message()
            self.__serializer.serialize_for_all(game_state_message)

    def __update_gamestate(self, delta_time: float) -> None:
        """Updates the game state by one tick.
        
        Args:
            delta_time (float): Time in seconds passed since last gamestate update.
        """
        for ball in self.__game_state.balls:
            self.__move_ball(ball, delta_time)
            collision_happened = self.__calculate_collisions(ball)
            if collision_happened:
                self.__move_ball(ball, delta_time)
            self.__speed_up_ball(ball, delta_time)
            self.__check_if_ball_is_lost(ball)

    def __move_ball(self, ball: Ball, delta_time: float):
        """Calculates the next position of a ball with its current velocity.

        Args:
            ball (Ball): Ball to move.
            delta_time (float): Time in seconds passed since last gamestate update.
        """
        if ball.consecutive_border_bounces > ball.MAX_CONSECUTIVE_BORDER_BOUNCES:
            random_vec = math_util.get_random_direction_vector()
            velocity_vector = [(1-ball.RANDOM_VELOCITY_VECTOR_INFLUENCE) * ball.velocity_vector[0] + 
                               ball.RANDOM_VELOCITY_VECTOR_INFLUENCE * random_vec[0],
                               (1-ball.RANDOM_VELOCITY_VECTOR_INFLUENCE) * ball.velocity_vector[1] + 
                               ball.RANDOM_VELOCITY_VECTOR_INFLUENCE * random_vec[1]]
            ball.velocity_vector[:] = math_util.normalize_vector(velocity_vector)
            ball.bounce_with_platform()
        ball.position[0] += ball.velocity_vector[0] * ball.velocity * delta_time
        ball.position[1] += ball.velocity_vector[1] * ball.velocity * delta_time


    def __speed_up_ball(self, ball: Ball, delta_time: float) -> None:
        """Accelerates the ball.

        Args:
            ball (Ball): The ball to accelerate
            delta_time (float): Delta time
        """
        ball.velocity += ball.SPEED_UP_PER_SECOND * delta_time

    def __calculate_collisions(self, ball: Ball) -> bool:
        """Calculates collisions of a ball.

        Args:
            ball (Ball): Ball which should collide.

        Returns:
            bool: Collision happened
        """
        collision_happened = False
        for i in range(self.__game_state.number_of_players):
            player = self.__game_state.players[i]
            # check collision with player platform
            if player.remaining_lives > 0:
                platform = player.platform
                platform_position = math_util.get_cartesian_coordinate(self.__game_state.CENTER, self.__game_state.RADIUS + platform.offset, platform.angle)
                collision_happened = self.__process_collision_with_circle(ball, platform_position, platform.radius)
                if collision_happened:
                    ball.bounce_with_platform()
            # check collision with game board border
            else:
                angle, radius = math_util.get_polar_coordinate(self.__game_state.CENTER, ball.position)
                angle = (angle + 2 * math.pi) % (2 * math.pi)
                min_angle = math_util.get_angle_from_message_angle(self.__game_state.number_of_players, i, 0.)
                max_angle = math_util.get_angle_from_message_angle(self.__game_state.number_of_players, i, 1.)
                if angle > min_angle and angle < max_angle:
                    collision_happened = self.__process_collision_with_inner_circle(ball, self.__game_state.CENTER, self.__game_state.RADIUS)
                    if collision_happened:
                        ball.bounce_with_border()
            if collision_happened:
                break
        if not collision_happened:
            for i in range(self.__game_state.number_of_players):
                # check collision with area limiting bumper
                boundary_angle = math_util.get_angle_from_message_angle(self.__game_state.number_of_players, i, 0.)
                boundary_position = math_util.get_cartesian_coordinate(self.__game_state.CENTER, self.__game_state.RADIUS, boundary_angle)
                collision_happened = self.__process_collision_with_circle(ball, boundary_position, self.__game_state.boundary_radius)
                if collision_happened:
                    ball.bounce_with_border()
                    break
        return collision_happened
    
    def __process_collision_with_circle(self, ball: Ball, circle_center: List[float], circle_radius: float) -> bool:
        """Calculates possible collision of a Ball with a circle.

        Args:
            ball (Ball): Ball which should collide.
            circle_center (List[float]): Center position of the circle.
            circle_radius (float): Radius of the circle.
        
        Returns:
            bool: Collision happened
        """
        collision = math_util.check_collision_of_circles(ball.position, ball.RADIUS, circle_center, circle_radius)
        correct_direction = math_util.check_direction_of_collision(ball.position, ball.velocity_vector, circle_center)
        if collision and correct_direction:
            collision_normal_vector = math_util.get_vector_from_point_to_point(circle_center, ball.position)
            collision_normal_vector = math_util.normalize_vector(collision_normal_vector)
            ball.velocity_vector = math_util.mirror_vector(ball.velocity_vector, collision_normal_vector)
            return True
        else:
            return False
    
    def __process_collision_with_inner_circle(self, ball: Ball, circle_center: List[float], circle_radius: float) -> bool:
        """Calculates possible collision of a Ball with a circle. The ball is inside the circle

        Args:
            ball (Ball): Ball which should collide.
            circle_center (List[float]): Center position of the circle.
            circle_radius (float): Radius of the circle.
        
        Returns:
            bool: Collision happened
        """
        if not math_util.check_collision_of_circles(ball.position, ball.RADIUS, circle_center, circle_radius - 2 * ball.RADIUS):
            collision_normal_vector = math_util.get_vector_from_point_to_point(ball.position, circle_center)
            collision_normal_vector = math_util.normalize_vector(collision_normal_vector)
            ball.velocity_vector = math_util.mirror_vector(ball.velocity_vector, collision_normal_vector)
            return True
        else:
            return False
    
    def __check_if_ball_is_lost(self, ball: Ball):
        """Checks whether the ball is outside the game field and decreases the lifes of the coresponding player.

        Args:
            ball (Ball): The ball to check.
        """
        if not math_util.check_collision_of_circles(ball.position, ball.RADIUS, self.__game_state.CENTER, self.__game_state.RADIUS):
            angle, radius = math_util.get_polar_coordinate(self.__game_state.CENTER, ball.position)
            # map angle into range [0, 2pi], as negative angles may lead to wrong player indices
            angle += 2 * math.pi
            angle %= 2 * math.pi
            player_index = int(angle / (2 * math.pi / self.__game_state.number_of_players))
            self.__game_state.players[player_index].remaining_lives -= 1
            if self.__game_state.players[player_index].remaining_lives <= 0:
                self.__game_state.players[player_index].remaining_lives = 0
                self.__check_win_condition()
            ball.position = self.__game_state.CENTER
            ball.velocity_vector = math_util.get_random_direction_vector()
            ball.velocity = ball.START_VELOCITY

    def __check_win_condition(self) -> None:
        """Checks if a player has won the game."""
        number_of_player_alive = 0
        for player in self.__game_state.players:
            if player.remaining_lives > 0:
                number_of_player_alive += 1
        if number_of_player_alive <= 1:
            self.__stop()

    def __get_winner_properties(self) -> Tuple[List[int], Union[int, None]]:
        """
        Determines the ids and remaining lives of those players with most remaining lives. Returns empty list and None, 
        if there are no players with remaining lives.

        Returns:
            Tuple[List[int], Union[int, None]]: Winners' ids and number of remaining lives
        """
        
        max_remaining_lives = max([player.remaining_lives for player in self.__game_state.players])
        if max_remaining_lives == 0:
            return [], None
        
        winner_ids = [player.player_id for player in self.__game_state.players if player.remaining_lives == max_remaining_lives]
        return winner_ids, max_remaining_lives

    def stop_server(self) -> None:
        if self.__game_is_running is True:
            self.__stop()
        self.__serializer.shutdown_network()
