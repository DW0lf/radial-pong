#!/usr/bin/env python3

from shared.serialization.serializable_message_type import SerializableMessageType
from shared.serialization.base_serializer import BaseSerializer
from server.network.network_handler import ServerNetworkHandler


class Serializer(BaseSerializer):
    """Implements interface for serializer in server enviroments."""
    
    def __init__(self, game: 'Game') -> None:
        """
        Initializes Serializer.
        
        Args:
            game (server.game.game.Game): Server-side game instance
        
        Raises:
            ConnectionError: binding of socket and thereby instancing of 
                ServerNetworkHandler failed
        """
        super().__init__()
        self.game = game
        # the instancing of ServerNetworkHandler may raise an ConnectionError if the 
        # binding of the socket fails; this error is not catched here, but in 
        # Game (where Serializer is intialized)
        self.network_handler = ServerNetworkHandler(self)
        
    @property
    def network_handler(self) -> ServerNetworkHandler:
        """
        Getter-method for ServerNetworkHandler instance.

        Returns:
            ServerNetworkHandler: server network handler instance, managing connection to client
        """
        
        return self.__network_handler
    
    @network_handler.setter
    def network_handler(self, network_handler: ServerNetworkHandler) -> None:
        """Setter-method for ServerNetworkHandler instance.

        Args:
            network_handler (ServerNetworkHandler): server network handler instance, managing connection to client
        """
        
        self.__network_handler = network_handler
    
    @property
    def game(self) -> 'Game':
        """
        Getter-method for Game instance.

        Returns:
            Game: Game instance, implementing game logic
        """
        
        return self.__game
    
    @game.setter
    def game(self, game: 'Game') -> None:
        """Setter-method for Game instance.

        Args:
            game (Game): Game instance, implementing game logic
        """
        
        self.__game = game
    
    def deserialize(self, content_data: bytes, player_id: int) -> None:
        """
        Deserializes data according to the included message type and updates game.

        Args:
            content_data (bytes): Data to be deserialized
            player_id (int): Player's identifier [0, 255]
        """
        
        # get message data
        message = self._get_message(content_data, player_id)
        
        # call updating method of game layer
        self.__game.update(message)
    
    def serialize_for_one(self, message: SerializableMessageType, player_id: int) -> None:
        """
        Serializes data according to the given message type and sends to the given player.

        Args:
            message (SerializableMessageType): Message to be serialized and sent
            player_id (int): Identifier of the player that is to receive the massage [0, 255]
        """
        
        # get content bytes
        content_bytes = self._get_content_bytes(message)
        
        # call sending method of network layer
        self.__network_handler.send(content_bytes, player_id)
    
    
    def serialize_for_all(self, message: SerializableMessageType) -> None:
        """
        Serializes data according to the given message type and sends to all players.

        Args:
            message (SerializableMessageType): Message to be serialized and sent
        """
        
        # get content bytes
        content_bytes = self._get_content_bytes(message)
        
        # call sending method of network layer
        self.__network_handler.send_to_all(content_bytes)

    def shutdown_network(self) -> None:
        if self.network_handler.socket_active() is True:
            self.network_handler.close_connection()
            
    def print_statistics(self) -> None:
        """Prints connection statistics as a table."""
        
        self.__network_handler.print_statistics()