#!/usr/bin/env python3

"""Implements the network communication layer on the server side."""

import queue
import sys
import os
sys.path.append(os.getcwd())
import socket
import time
import selectors
from queue import Queue
from types import SimpleNamespace
from typing import Tuple, List

from server.network.connection_handler import ServerConnectionHandler
from server.network.network_statistics import NetworkMonitor
from shared.network.network_handler import BaseNetworkHandler

SERVER_SLEEP = 1e-4 


class Client:
    """Represents a client and its connection.
    """
    def __init__(self, id: int, ip_addr: str, port: int) -> None:
        """Initialize container representing a client and its connection.

        Args:
            id (int): client ID provided by ServerNetworkHandler
            ip_addr (str): ip address of connected client
            port (int): port of connected client
        """
        self.__id = id
        self.__recv_queue = Queue()
        self.__send_queue = Queue(10)
        self.__active = True
        self.__ip_address = ip_addr
        self.__port = port
    
    @property
    def recv_queue(self) -> queue.Queue:
        """Getter for receive queue.

        Returns:
            queue.Queue: receive queue for client
        """
        return self.__recv_queue
    
    @recv_queue.setter
    def recv_queue(self, new_queue: Queue) -> None:
        """Setter for client's receive queue.

        Args:
            new_queue (Queue): new queue to attach to client
        """
        self.__recv_queue = new_queue

    @property
    def send_queue(self) -> queue.Queue:
        """Getter for send queue.

        Returns:
            queue.Queue: send queue for client
        """
        return self.__send_queue

    @send_queue.setter
    def send_queue(self, new_queue: Queue) -> None:
        """Setter for client's send queue.

        Args:
            new_queue (Queue): new queue to attach to client
        """
        self.__send_queue = new_queue
    
    @property
    def id(self) -> int:
        """Getter for player's ID.

        Returns:
            int: player's ID
        """
        return self.__id

    @property
    def active(self) -> bool:
        """Returns if player is stil active or disconnected.

        Returns:
            bool: status of player connection
        """
        return self.__active
    
    @active.setter
    def active(self, active_status: bool) -> None:
        """Setter for active status 

        Args:
            active_status (bool): new activity status
        """
        self.__active = active_status
    
    @property
    def ip_address(self) -> str:
        """Getter for ip address of client.
        
        Returns:
            str: client's ip address
        """
        return self.__ip_address

    @property
    def port(self) -> int:
        """Getter for port of client's socket.

        Returns:
            int: client socket's port
        """
        return self.__port
        
class ServerNetworkHandler(BaseNetworkHandler):
    """Specializes the BaseNetworkHandler for the server side."""
    
    MAX_CONNECTION_COUNT = 32
    
    def __init__(self, serializer: 'Serializer') -> None:
        """Initializes super-class, create server-specific attributes, bind socket and start listener thread as well as handler thread.

        Args:
            serializer (Serializer): current Serializer instance
        Raises:
            ConnectionError: binding of socket failed
        """
        super().__init__(serializer)
        
        # server-specific attributes
        self.__ip_address = socket.gethostbyname(socket.gethostname())
        self.__port = self.__find_port()
        self.__connection_handlers = list()
        self.__pending = False
        self.__clients = dict()
        self.__used_ids = set()
        
        # setup network monitor
        self.__monitor = NetworkMonitor()

        # set up and start listener
        self._start_listener()

        # set up socket
        status = self.__setup_socket()
        
        if not status:
            # binding of socket failed, free selector and stop listener
            self._close()
            raise ConnectionError("Binding of socket failed for port %d." % self.__port)

        # start run method in a seperate thread
        self._start_handler()
    
    def __setup_socket(self) -> bool:
        """Setup server socket, set to listening and add to multiplexer.
        
        Returns:
            bool: status on creation of socket (True: socket set up successfully,
                False: socket binding failed)

        Raises:
            AttributeError: Listener instance not attached
        """
        # check if listener is attached
        if self.listener is None:
            raise AttributeError("No listener attached to this ServerNetworkHandler. " 
                "Set object's attribute listener to attach one.")

        # create socket and set listening
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self._sock.bind((self.__ip_address, self.__port))
        except OSError:
            return False
        self._sock.listen()
        self._running = True
        self._sock.setblocking(False)

        # register main server socket for multiplexing
        self.selector.register(self._sock, ServerNetworkHandler.READ_WRITE_EVENT, data=None)
        print("Listening on %s:%d" % (self.__ip_address, self.__port))

        return True
    
    def run(self) -> None:
        """Overwrite BaseConnectionHandler's run method.
        Listens for new connection request and executes sending and receiving 
        data for establishes connections. This method should be run in another 
        thread.
        """
        self.__pending = True
        while self._running:
            # timeout is necessary to ensure, that the server does not run in
            # a deadlock when a client is closed 
            events = self.selector.select(timeout=None)
            time.sleep(SERVER_SLEEP) # this is necessary due to selectors' select 
            # function returning immediately, because the sockets are always 
            # ready to write and thereby this function would poll
            
            for idx, event in enumerate(events):
                key, mask = event
                if key.data is None:
                    # ensure that only a specific number of sockets are open
                    if sum(self.__used_ids) < self.MAX_CONNECTION_COUNT:
                        # accept connection request and register client-specific socket
                        new_key, ip_addr = self.__accept_wrapper(key.fileobj)
                        player_id = self.__add_client(ip_addr)

                        # add client to NetworkMonitor's tracking
                        self.__monitor.register_client(player_id)

                        new_key.data.player_id = player_id
                        sendq, recvq = self._send_buffers[player_id], self._recv_buffers[player_id]
                        self.__connection_handlers.append(ServerConnectionHandler(self.selector, sendq, recvq, self.condition, player_id, self.__monitor))
                    else:
                        # reject connection request
                        key.fileobj.close()

                else:
                    # manage existing connection
                    self.__connection_handlers[key.data.player_id].handle(key, mask)
                    
                    # remove client if server received 10 contious messages for which CRC failed
                    err_count = self.__monitor.continiously_errors(player_id)

                    if err_count >= 10:
                        self.__remove_player(key.data.player_id)
                        
        self.__pending = False
        
    def print_statistics(self) -> None:
        """Prints connection statistics as a table."""
        
        statistics = self.__monitor.format_statistics()
        print(statistics)

    def __find_port(self, port_range: range=range(8181, 8281)) -> int:
        """Returns a free port for a new TCP server.
        
        Args:
            port_range(range): Port range to be tested (default: 8181 to 8280)
        
        Returns:
            int: free port
        
        Raises:
            ValueError: No available port in the given range
        """
        for port in port_range:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
                # test if port is blocked
                if sock.connect_ex((self.ip_address, port)):
                    return port     
        raise ValueError("Port %d and the following %d ones are in use. Try another port range." 
                         % (port_range[0], len(port_range)-1))
    
    def __accept_wrapper(self, sock: socket.socket) -> Tuple[selectors.SelectorKey, str]:
        """Accepts incoming conncetion requests and registers the socket in the selector.

        Args:
            sock(socket.socket): the newly requested socket

        Returns:
            key(selectors.SelectorKey): newly created selectors element
            addr(str): The IP address
        """
        conn, addr = sock.accept()
        conn.setblocking(False)
        data = SimpleNamespace(player_id = None)
        key = self.selector.register(conn, ServerNetworkHandler.READ_WRITE_EVENT, data=data)
        return key, addr    
    
    def send(self, content: bytes, player_id: int) -> None:
        """Overwrites BaseNetworkHandler's send method to send message to one specific client. Puts serialized message into the specific sending queue.

        Args:
            content(bytes): serialized message to send
            player_id(Int): id of player to send message to
        
        Raises: 
            IndexError: if player_id is not valid
            TypeError: content is not of type bytes        
        """
        if not isinstance(content, bytes):
            raise TypeError("Argument content must be of type bytes.")
        if player_id not in self.__used_ids:
            raise IndexError("There is no player with ID %d" % player_id)

        # check if socket of specified player is still opened
        if not self.__clients[player_id].active:
            # socket is closed
            return
        
        if self.__clients[player_id].send_queue.full():
            # socket for client connection is closed (this solution is quite
            # hacky, but because the server checks every second if there is 
            # data to send and treats all clients the same, no send_buffer 
            # would overflow if the socket is not closed)
                
            # remove queues for messages to send and receive
            self.__remove_player(player_id)
        else:
            # socket for client connection is still opened
            self._send_message(content, player_id)

    def send_to_all(self, content: bytes) -> None:
        """Put serialized message in sending queue for all players.

        Args:
            serialized_object(SerializableMessageType): serialized message to send
        """

        for player in self.__clients.values():
            if player.active:
                self.send(content, player.id)
    
    def __remove_player(self, player_id: int) -> None:
        """Unregister player if its socket was closed.

        Args:
            player_id(int): zero-based ID of player
        """
        if player_id  in self.__used_ids:
            self.__clients[player_id].active = False
            self.__clients[player_id].recv_queue = None
            self.__clients[player_id].send_queue = None
            self.__used_ids.remove(player_id)

    def __add_client(self, addr: str) -> int:
        """Add a new client to the server and update the buffers.

        Args:
            addr(str): the IP address of the client
        
        Returns:
            player_id(int): the ID of the added client
        
        Raises:
            IndexError: there are already a maximal number of players connected
        """
        player_id = None
        for i in range(self.MAX_CONNECTION_COUNT):
            if i not in self.__used_ids:
                self.__used_ids.add(i)
                player_id = i
                break
        
        if player_id is None:
            raise IndexError("There are already %d clients connected." % 
                             self.MAX_CONNECTION_COUNT) 
        
        ip_addr, port = addr
        new_client = Client(player_id, ip_addr, port)
        self.__clients[player_id] = new_client
        
        if len(self._send_buffers) < player_id + 1:
            self._send_buffers.append(new_client.send_queue)
            self._recv_buffers.append(new_client.recv_queue)
        else:
            self._send_buffers[player_id] = new_client.send_queue
            self._recv_buffers[player_id] = new_client.recv_queue
        
        print("Add player %d with IP %s" % (player_id, addr))

        return player_id
    
    def socket_active(self) -> bool:
        """Returns True if socket is active, returns False if socket is stoped.
        
        Returns:
            bool: state of socket
        """
        return self.__pending

    def get_client_ip(self, player_id: int) -> str:
        """Getter for the ip adress of a specific client.

        Args:
            player_id(int): zero-based client ID

        Returns:
            str: ip adress of client
        
        Raises:
            IndexError: player_id is not valid
        """
        if player_id not in self.__used_ids:
            raise IndexError("There is no player with ID %d" % player_id)
        return self.__clients[player_id].ip_address
    
    def get_client_port(self, player_id: int) -> int:
        """Getter for the ip adress of a specific client.

        Args:
            player_id(int): zero-based client ID

        Returns:
            int: port of client
        
        Raises:
            IndexError: player_id is not valid
        """
        if player_id not in self.__used_ids:
            raise IndexError("There is no player with ID %d" % player_id)
        return self.__clients[player_id].port
    
    def get_client_state(self, player_id: int) -> bool:
        """Returns connection status of specified player. If the player is still
        connected 'True' is returned, otherwise 'False'.
        
        Args:
            player_id(int): zero-based client ID
        
        Returns:
            bool: connection status of client
        """
        if player_id not in self.__clients.keys():
            return False
        
        return self.__clients[player_id].active
    
    @property
    def ip_address(self) -> str:
        """Getter for ip address.
        
        Returns:
            str: ip address of server
        """
        return self.__ip_address
    
    @property
    def port(self) -> int:
        """Getter for server's TCP port.
        
        Returns:
            int: port of TCP server
        """
        return self.__port
    
    @property
    def receive_buffers(self) -> List[Queue]:
        """Getter for list of all receive queues.
        
        Returns:
            List(Queue): list of queues containing the received messages for all connected clients
        """
        return self._recv_buffers

    @property
    def monitor(self) -> NetworkMonitor:
        """Getter for network monitor.

        Returns:
            NetworkMonitor: network monitor instance of network handler
        """
        return self.__monitor    
