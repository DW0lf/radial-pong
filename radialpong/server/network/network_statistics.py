#!/usr/bin/env python3

"""Implements the network monitor on the server side."""

import sys
import os
from tabulate import tabulate
sys.path.append(os.getcwd())


class StatisticsContainer():
    """Container for statistics on network communication for a single client.
    
    Attributes:
        recv_mess(int): number of received valid messages from client
        recv_bytes(int): number of received bytes(incl. bytes for header) from 
            client (bytes belonging to valid messages)
        sent_mess(int): number of messages sent to the client
        sent_bytes(int): number of bytes (incl. bytes for header) sent to client
        err_mess(int): number of invalid messages
        continious_err(int): number of erros in a row """

    def __init__(self) -> None:
        """Initialize network statistics container.
        """
        self.recv_mess = None
        self.sent_mess = None
        self.recv_bytes = None
        self.sent_bytes = None
        self.err_mess = None
        self.continious_err = None
        self.reset()
    
    def reset(self) -> None:
        """Resets statistics."""
        
        self.recv_mess = 0
        self.sent_mess = 0
        self.recv_bytes = 0
        self.sent_bytes = 0
        self.err_mess = 0
        self.continious_err = 0

class NetworkMonitor():
    """Implements network monitor for server. Stores client-specific statistics 
    on network communication in separate containers.
    """
    def __init__(self) -> None:
        """Initialize a NetworkMonitor object."""
        self.__client_list = dict()
        
    def format_statistics(self) -> str:
        """
        Formats statistics on the client-server connections as a table. Each row contains statistics of a single 
        client-server connection. The last row contains summed statistics of the overall game. Resets statistics as 
        well.
        
        Returns:
            str: Formatted statistics
        """
        
        headers = [
            "",
            "Recv. Messages", 
            "Sent Messages", 
            "Recv. Bytes", 
            "Sent Bytes", 
            "Err. Messages (abs)", 
            "Err. Messages (rel)"
        ]
        
        # get statistics data in the correct format (list of lists)
        stat_data = []
        for player_id, player_statistics in self.__client_list.items():
            if player_statistics.recv_mess:
                err_percentage = 100. / player_statistics.recv_mess * player_statistics.err_mess
            else:
                err_percentage = 0
            row_data = [
                "Player %d" % player_id, 
                player_statistics.recv_mess,
                player_statistics.sent_mess,
                player_statistics.recv_bytes,
                player_statistics.sent_bytes,
                player_statistics.err_mess,
                "%.2f %%" % (err_percentage)
            ]
            stat_data.append(row_data)
        
        # create row that contains summed statistics across players
        all_recv_messages = self.all_recv_packages()
        all_sent_messages = self.all_sent_packages()
        all_recv_bytes = self.all_recv_bytes()
        all_sent_bytes = self.all_sent_bytes()
        all_err_messages = self.all_err_packages()
        stat_data.append([
            "Σ", 
            all_recv_messages, 
            all_sent_messages,
            all_recv_bytes,
            all_sent_bytes,
            all_err_messages,
            "%.2f %%" % (100. / all_recv_messages * all_err_messages)
        ])
        
        raw_table = tabulate(stat_data, headers, tablefmt="psql")
        rows = raw_table.split("\n")
        seperator = rows[-1]
        rows.insert(-2, seperator)
        rows = [row + "\n" for row in rows]
        
        self.__reset_statistics()
        
        return "".join(rows)
        
    def __reset_statistics(self) -> None:
        """Resets connection statistics for each client."""
        
        for player_statistics in self.__client_list.values():
            player_statistics.reset()
    
    def register_client(self, client_id: int) -> None:
        """Method to register a new player. 
        
        Args:
            client_id: client ID also used in ServerNetworkHandler
        """
        self.__client_list[client_id] = StatisticsContainer()
    
    def increment_recv_counter(self, player_id : int, bytes_count: int) -> None:
        """Increment the recv counters of a specific client.
        
        Args:
            player_id(int): ID of the player
            bytes_count(int): number of received bytes

        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        self.__client_list[player_id].recv_mess += 1
        self.__client_list[player_id].recv_bytes += bytes_count

        # reset counter for continiously appeared errors
        self.__client_list[player_id].continious_err = 0

    def increment_sent_counter(self, player_id : int, bytes_count: int) -> None:
        """Increment the sent counters of a specific client.
        
        Args:
            player_id(int): ID of the player
            bytes_count(int): number of bytes sent
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        self.__client_list[player_id].sent_mess += 1
        self.__client_list[player_id].sent_bytes += bytes_count

    def increment_err_counter(self, player_id : int) -> None:
        """Increment the err counter of a specific client and return the new value.
        
        Args:
            player_id(int): ID of the player
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        self.__client_list[player_id].err_mess += 1
        self.__client_list[player_id].continious_err += 1
    
    def received_packages_client(self, player_id : int) -> int:
        """Get number of received messages from a specific client.
        
        Args:
            player_id(int): ID of the player
        
        Returns:
            int: number of received messages
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        return self.__client_list[player_id].recv_mess

    def sent_packages_client(self, player_id : int) -> int:
        """Get number of sent messages from a specific client.
        
        Args:
            player_id(int): ID of the player
        
        Returns:
            int: number of sent messages
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        return self.__client_list[player_id].sent_mess

    def err_packages_client(self, player_id : int) -> int:
        """Get number of compromised messages from a specific client.
        
        Args:
            player_id(int): ID of the player
        
        Returns:
            int: number of invalid messages
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        return self.__client_list[player_id].err_mess

    def received_bytes_client(self, player_id : int) -> int:
        """Get number of received bytes (belonging to valid messages) from a 
        specific client.
        
        Args:
            player_id(int): ID of the player
        
        Returns:
            int: number of received bytes 
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        return self.__client_list[player_id].recv_bytes

    def sent_bytes_client(self, player_id : int) -> int:
        """Get number of sent bytes from a specific client.
        
        Args:
            player_id(int): ID of the player
        
        Returns:
            int: number of sent bytes 
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)

        return self.__client_list[player_id].sent_bytes
    
    def continiously_errors(self, player_id: int) -> int:
        """Returns number of continiously appeared errors.

        Args:
        player_id(int): ID of the player

        Returns:
            int: number of continious errors
        
        Raises:
            IndexError: Client with given player_id not connected
        """
        self.__check_player_id(player_id)
        
        return self.__client_list[player_id].continious_err

    def all_recv_packages(self) -> int:
        """Get sum over all received packages.

        Returns:
            int: total number of received packages
        """
        recv = 0
        for client in self.__client_list.values():
            recv += client.recv_mess
        return recv
    
    def all_sent_packages(self) -> int:
        """Get sum over all sent packages.
        
        Returns:
            int: total number of sent packages
        """
        send = 0
        for client in self.__client_list.values():
            send += client.sent_mess
        return send
    
    def all_err_packages(self) -> int:
        """Get sum over all compromised received packages.
        
        Returns:
            int: total number of errors
        """
        err = 0
        for client in self.__client_list.values():
            err += client.err_mess
        return err
    
    def all_recv_bytes(self) -> int:
        """Get sum over all received bytes.

        Returns:
            int: total number of received bytes
        """
        
        byte_count = 0
        for client in self.__client_list.values():
            byte_count += client.recv_bytes
        return byte_count
    
    def all_sent_bytes(self) -> int:
        """Get sum over all sent bytes.

        Returns:
            int: total number of sent bytes
        """
        
        byte_count = 0
        for client in self.__client_list.values():
            byte_count += client.sent_bytes
        return byte_count
    
    def __check_player_id(self, player_id: int) -> None:
        """Checks if client with given ID is registered. If it is not registered 
        an IndexError is raised.

        Args:
            player_id(int): zero-based client ID
        
        Raises: 
            IndexError: Client with given player_id not connected
        """
        if player_id not in self.__client_list.keys():
            raise IndexError("Player with ID %d not connected." % player_id)
    