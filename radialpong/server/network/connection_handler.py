#!/usr/bin/env python3

"""Specializes BaseConnectionHandler to support acquiring of statistics on server 
side."""

import selectors
import threading
import socket
from queue import Queue

from shared.network.connection_handler import BaseConnectionHandler
from server.network.network_statistics import NetworkMonitor

class ServerConnectionHandler(BaseConnectionHandler):
    """Spezialized ConnectionHandler for server, which extends the 
    BaseConnectionHandler by a NetworkMonitor to acquire statistics and stores 
    the client ID. In ServerNetworkHandler this class is separatly instanced 
    for every client and afterwards only used for this specific client connection.
    """
    def __init__(self, selector: selectors.DefaultSelector, sendq: Queue, 
                 recvq: Queue, condition: threading.Condition, player_id: int, 
                 monitor:NetworkMonitor) -> None:
        """Initialize ServerConnectionHandler.

        Args:
            selector(selectors.DefaultSelector): multiplexer object for socket
            sendq(queue.Queue): queue to take messages to send from
            recvq(queue.Queue): queue to put received messages to
            condition(threading.Condition): condition to notify on message receive
            player_id (int): ID of client this connection handler is responsible
                for
            monitor (NetworkMonitor): object to acquire statistics about network
                communication
        """
        super().__init__(selector, sendq, recvq, condition)
        self.__monitor = monitor
        self.__player_id = player_id
    
    def _on_failed_crc(self) -> None:
        """Overwrites BaseConnectionHandler's method.
        Is called when CRC failed. Increments monitors error count and object's
        receive counter.
        """
        self.__monitor.increment_err_counter(self.__player_id)
        return super()._on_failed_crc()
    
    def _store_future_message(self, package_counter: int, content: bytes) -> None:
        """Overwrites BaseConnectionHandler's method.
        Stores messages that were received to early in a queue and increments monitor's receive counter.

        Args:
            package_counter (int): number of package
            content (bytes): decomposed content
        """
        self.__monitor.increment_recv_counter(self.__player_id, len(content) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH)
        return super()._store_future_message(package_counter, content)
    
    def _store_current_message(self, content: bytes) -> None:
        """Overwrites BaseConnectionHandler's method.
        Puts decomposed content of to right time received message to output 
        queue, which is emptied by the listener. Increments object's receive
        counter, monitor's receive counter and checks if messages in waiting queue may be put to output 
        queue.

        Args:
            content (bytes): decomposed content
        """
        self.__monitor.increment_recv_counter(self.__player_id, len(content) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH)
        return super()._store_current_message(content)
    
    def _send_package(self, data_to_send: bytes, sock: socket.socket) -> None:
        """Overwrites BaseConnectionHandler's method.
        Sends the given data, increments object's send counter and returns 
        number of sent bytes.
        
        Args:
            data_to_send (bytes): content to send 
            sock (socket.socket): socket to use for sending
        """
        super()._send_package(data_to_send, sock)
        self.__monitor.increment_sent_counter(self.__player_id, len(data_to_send) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH)
    
    @property
    def monitor(self) -> NetworkMonitor:
        """Getter for monitor attribute.

        Returns:
            NetworkMonitor: NetworkMonitor object providing the network statistics
        """
        return self.__monitor
