#!/usr/bin/env python3

import time
import os
import sys
sys.path.append(os.getcwd()+"/radialpong")
from server.game.game import Game
import signal

class GameServerRunner():
    def __init__(self):
        self.game = Game()

    def signal_handler(self, signal, frame):
        self.game.stop_server()
        print("Server shutdown successful")
        sys.exit(0)
    
    def start(self):
        self.game.start_server()
    
    def stop(self):
        self.game.stop_server()

if __name__ == "__main__":

    server = GameServerRunner()
    signal.signal(signal.SIGINT, server.signal_handler)
    try:
        server.start()
        while True:
            time.sleep(60*60) # sleep an hour
    except BaseException as e:
        server.stop()
        print(e)
