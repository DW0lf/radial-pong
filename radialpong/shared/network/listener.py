#!/usr/bin/env python3

"""Implements a listener for received packages."""

import threading
from typing import List
from queue import Queue


class Listener():
    """Listerner for received packages. This listener should be run
    in another thread, next to the main thread and the network communication thread.   
    If any message is received from a client, this thread is woken up und calls 
    the Serializer to deserialize the received messages.
    This class is instanced in the BaseNetworkHandler.
    
    Usage:
        1. Instanciate Listener class.
        2. Execute method listen() in another thread(e. g.
            listener_thread = threading.Thread(target=listener.run)
            listener_thread.daemon = True    # optional, sets thread as daemon
            listener_thread.start()
        3. Now the network communication thread can call condition's method 
        notify() to signal the waiting thread that a message is avaliable."""
    
    def __init__(self, queues: List[Queue], condition: threading.Condition, 
        serializer: 'Serializer') -> None:
        """Instanciate listener object.
        
        Args:
            queues(List(Queue)): list queues to get received messages from
            condition(threading.Condition): condition to wait for
            serializer(Serializer): Serializer object to call for message deserialization
        """
        self.__condition = condition
        self.__recvq = queues
        self.__serializer = serializer
        self.__running = False
    
    def listen(self) -> None:
        """Listens for received packages (depends on the condition). Calls 
        Serializer to deserialize the message and update the game.
        """
        self.__running = True
        messages_available = True
        while self.__running:
            # wait for received message
            with self.__condition:
                # wait for notify; timeout is set to 1s to ensure that the messages are processed even if the notify was missed
                self.__condition.wait(timeout=1)
            
            messages_available = True
            while messages_available: 
                messages_available = False
                for idx_player, buffer in enumerate(self.__recvq):
                    if buffer is not None:
                        # ensure that player is active
                        if not buffer.empty():
                            # process first message from the queue
                            self.__serializer.deserialize(buffer.get(), idx_player)
                            

                            if not buffer.empty():
                                # there are more elements in the queue to send
                                messages_available = True
                    
    def stop(self) -> None:
        """Stops the listener, which is running on another thread.
        """
        self.__running = False

    
    def __del__(self) -> None:
        """Implements the deconstructor.
        """
        self.stop()
