#!/usr/bin/env python3

from enum import IntEnum
import struct
from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType


class ConnectionStatus(IntEnum):
    """Implements connection statuses which are sent to the client as part of connection confirmation."""
    
    # connection failed
    FAILED = 0
    # connection successful, waiting for the game to start
    SUCCESSFUL_WAITING = 1
    # connection successful, game is already running
    SUCCESSFUL_RUNNING = 2
    
    
class ConnectionConfirmationMessage(SerializableMessageType):
    """Implements message being sent from server to client to confirm client's connection request."""
    
    # valid values for connection status identifier
    __VALID_CONNECTION_STATUSES = tuple(item.value for item in ConnectionStatus)
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 2
    
    def __init__(self, player_id: int, connection_status: ConnectionStatus) -> None:
        """
        Initializes ConnectionConfirmationMessage.

        Args:
            player_id (int): Player's identifier [0, 255]
            connection_status (ConnectionStatus): Connection status' identifier
        """
        
        self.player_id = player_id
        self.connection_status = connection_status
        
    @property
    def player_id(self) -> int:
        """
        Getter-method for attribute \"player_id\".

        Returns:
            int: Player's identifier [0, 255]
        """
        
        return self.__player_id
    
    @player_id.setter
    def player_id(self, player_id: int) -> None:
        """
        Setter-method for attribute \"player_id\".

        Args:
            player_id (int)): Player's identifier [0, 255]

        Raises:
            ValueError: Player's identifier must be in range [0, 255]
        """
    
        # check boundaries for player_id
        upper_boundary = 2**(8*self._PLAYER_ID_BYTES)-1
        if player_id < 0 or player_id > upper_boundary:
            raise ValueError("Argument \"player_id\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, 
                                                                                                   player_id))
        
        self.__player_id = player_id
    
    @property
    def connection_status(self) -> int:
        """
        Getter-method for attribute \"connection_status\".

        Returns:
            ConnectionStatus: Connection status' identifier
        """
        
        return self.__connection_status
    
    @connection_status.setter
    def connection_status(self, connection_status: ConnectionStatus) -> None:
        """
        Setter-method for attribute \"connection_status\".

        Args:
           connection_status (ConnectionStatus)): Connection status' identifier
        """
    
        self.__connection_status = connection_status
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # cast python integer types to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representations
        player_id_bytes = struct.pack("B", self.__player_id)
        connection_status_bytes = struct.pack("B", self.__connection_status)
        
        # return concatenation of bytes, according to the transmission protocol
        return connection_status_bytes + player_id_bytes
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'ConnectionConfirmationMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (2 bytes); transmitted value of connection status must be valid (in range [0, 2])
            
        Returns:
            ConnectionConfirmationMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data matches expectations according to the transmission protocol
        if len(serialized_data) != cls._CONNECTION_STATUS_BYTES + cls._PLAYER_ID_BYTES:
            raise ValueError("Argument \"serialized_data\" must be %d bytes (currently: %d)!" % 
                             (cls._CONNECTION_STATUS_BYTES + cls._PLAYER_ID_BYTES, len(serialized_data)))
        
        # according to the transmission protocol, connection_status is the first and player_id the second byte
        # bytes are implicitely casted to python integers
        # connection_status must be casted to enum type explicitely, therefore check validity first
        if serialized_data[0] not in cls.__VALID_CONNECTION_STATUSES:
            raise ValueError("Transmitted value of \"connection_status\" (currently: %d) is invalid!" % 
                             serialized_data[0])
        
        connection_status = ConnectionStatus(serialized_data[0])
        player_id = serialized_data[1]
        
        # return new class instance, which encapsulates the deserialized data
        return ConnectionConfirmationMessage(player_id, connection_status)