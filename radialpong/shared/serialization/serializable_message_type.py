 #!/usr/bin/env python3
 
from typing import Union
 
class SerializableMessageType:
    """Implements interface for serializable message type classes."""
    
    # identifier for message type
    _MESSAGE_TYPE_ID = 0
    # number of bytes being used for transmitted player id, according to the transmission protocol
    _PLAYER_ID_BYTES = 1
    # number of bytes being used for player count, according to the transmission protocol
    _PLAYER_COUNT_BYTES = 1
    # number of bytes being used for transmitted angle data, according to the transmission protocol
    _PLAYER_ANGLE_BYTES = 2
    # number of bits being used for the fracional part of angle's fixed point representation
    _PLAYER_ANGLE_FRAC_BITS = 15
    # number of bytes being used for player lives count, according to the transmission protocol
    _PLAYER_LIVES_BYTES = 1
    # number of bytes being used for ball count, according to the transmission protocol
    _BALL_COUNT_BYTES = 1
    # number of bytes being used for transmitted ball position data (1D), according to the transmission protocol
    _BALL_POSITION_BYTES = 2
    # number of bytes being used for transmitted connection status, according to the transmission protocol
    _CONNECTION_STATUS_BYTES = 1
    
    def __init__(self) -> None:
        """
        Initializes SerializableMessageType.

        Raises:
            NotImplementedError: Must be overridden for concrete serializable message type classes
        """
        
        raise NotImplementedError
    
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Raises:
            NotImplementedError: Must be overridden for concrete serializable message type classes

        Returns:
            bytes: Serialized data
        """
        
        raise NotImplementedError
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'SerializableMessageType':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]

        Raises:
            NotImplementedError: Must be overridden for concrete serializable message type classes
            
        Returns:
            SerializableMessageType: Deserialized data, being encapsulated in class instance
        """
        
        raise NotImplementedError
    
    @classmethod
    @property
    def MESSAGE_TYPE_ID(cls) -> int:
        """
        Getter-method for read-only class attribute \"MESSAGE_TYPE_ID\".

        Raises:
            NotImplementedError: Abstract class \"SerializableMessageType\" is not a concrete, hence valid message type

        Returns:
            int: Message type identifier of the concrete message type
        """
        
        if cls is SerializableMessageType:
            raise NotImplementedError
        else:
            return cls._MESSAGE_TYPE_ID