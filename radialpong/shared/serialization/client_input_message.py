#!/usr/bin/env python3

import struct
from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType


class ClientInputMessage(SerializableMessageType):
    """Implements message being sent from client to server to indicate client's input."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 7
    
    def __init__(self, player_angle: float, player_id: int = 0) -> None:
        """
        Initializes ClientInputMessage.

        Args:
            player_id (int, optional): Player's identifier [0, 255] (default 0)
            player_angle (float): New platform angle [0., 1.]
        """
        
        self.player_id = player_id
        self.player_angle = player_angle
        
    @property
    def player_id(self) -> int:
        """
        Getter-method for attribute \"player_id\".

        Returns:
            int: Player's identifier [0, 255]
        """
        
        return self.__player_id
    
    @player_id.setter
    def player_id(self, player_id: int) -> None:
        """
        Setter-method for attribute \"player_id\".

        Args:
            player_id (int)): Player's identifier [0, 255]

        Raises:
            ValueError: Player's identifier must be in range [0, 255]
        """
    
        # check boundaries for player_id
        upper_boundary = 255
        if player_id < 0 or player_id > upper_boundary:
            raise ValueError("Argument \"player_id\" must be in range [0, %d] (currently: %d)!" % (upper_boundary, 
                                                                                                   player_id))
        
        self.__player_id = player_id
        
    @property
    def player_angle(self) -> float:
        """
        Getter-method for attribute \"player_angle\".

        Returns:
            float: Platform angle [0., 1.]
        """
        
        return self.__player_angle
    
    @player_angle.setter
    def player_angle(self, player_angle: float) -> None:
        """
        Setter-method for attribute \"player_angle\".

        Args:
            player_angle (float): New platform angle [0., 1.]

        Raises:
            ValueError: Platform angle must be in range [0., 1.]
        """
    
        # check boundaries for angle argument
        if player_angle < 0. or player_angle > 1.:
            raise ValueError("Argument \"player_angle\" must be in range [0., 1.] (currently: %f)!" % player_angle)
        
        self.__player_angle = player_angle
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # convert 8 byte floating point datatype to integer representation of 2 byte fixed point datatype, where 
        # 15 bits are used for the fractional part and 1 bit is used for the integer part (since angle is in range 
        # [0., 1.])
        player_angle_int = int(self.__player_angle * (2**self._PLAYER_ANGLE_FRAC_BITS))
        
        # angle_int will always be in range [0, 2^15]!
        
        # cast python integer type to ctypes 2 byte unsigned integer type (abbr. "H") and return byte-wise data 
        # representation
        return struct.pack("H", player_angle_int)
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'ClientInputMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
        
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (2 bytes)
            TypeError: Player id is required
            
        Returns:
            ClientInputMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data matches expectations, according to the transmission protocol (2 bytes)
        if len(serialized_data) != cls._PLAYER_ANGLE_BYTES:
            raise ValueError("Argument \"serialized_data\" must be %d bytes (currently: %d)!" % 
                             (cls._PLAYER_ANGLE_BYTES, len(serialized_data)))
            
        # check, if player id is given
        if player_id is None:
            raise TypeError("Argument \"player_id\" must not be None!")
        
        # get ctypes 2 byte unsigned integer (abbr. "H") from byte-wise data representation (automatically cast to 
        # python integer type)
        player_angle_int = struct.unpack("H", serialized_data)[0]
        
        # convert python integer type, which is the integer representation of 2 byte fixed point datatype with 15 bits 
        # being used for the fractional part and 1 bit used for the integer part, to 8 byte floating point datatype
        
        player_angle = player_angle_int / (2**cls._PLAYER_ANGLE_FRAC_BITS)
        
        # return new class instance, which encapsulates the deserialized data
        return ClientInputMessage(player_angle, player_id)