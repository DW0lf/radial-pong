#!/usr/bin/env python3

import math
import struct
from typing import List, Tuple, Union

from shared.game.game_components import GameState
from shared.serialization.serializable_message_type import SerializableMessageType


class GameStateMessage(SerializableMessageType):
    """Implements message being sent from server to client, containing the current game state."""

    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 6
    
    def __init__(self, ball_positions: List[Tuple[float, float]], player_angles: List[float], 
                 player_lives: List[int]) -> None:
        """
        Initializes GameStateMessage.

        Args:
            ball_positions (List[Tuple[float, float]]): Positions of balls as (x,y) coordinate tuples [0., 65536.)
            player_angles (List[float]): Angles of players' platforms [0., 1.]
            player_lives (List[int]): Players' number of remaining lives [0, 255]
        """
        
        self.ball_positions = ball_positions
        self.player_angles = player_angles
        self.player_lives = player_lives
        
    @staticmethod
    def from_game_state(game_state: GameState) -> 'GameStateMessage':
        """
        Creates GameStateMessage from GameState instance.

        Args:
            game_state (GameState): GameState instance, containing current game state information

        Returns:
            GameStateMessage: GameStateMessage instance, encapsuling game state information
        """
        
        ball_positions = [tuple(ball.position[:2]) for ball in game_state.balls]
        player_angles = [player.platform.angle for player in game_state.players]
        player_lives = [player.remaining_lives for player in game_state.players]
        
        return GameStateMessage(ball_positions, player_angles, player_lives)
        
    @property
    def ball_positions(self) -> List[Tuple[float, float]]:
        """
        Getter-method for attribute \"ball_positions\".

        Returns:
            List[Tuple[float, float]]: Positions of balls as (x,y) coordinate tuples [0., 65536.)
        """
        
        return self.__ball_positions
    
    @ball_positions.setter
    def ball_positions(self, ball_positions: List[Tuple[float, float]]) -> None:
        """
        Setter-method for attribute \"ball_positions\".

        Args:
            ball_positions (List[Tuple[float, float]]): Positions of balls as (x,y) coordinate tuples [0., 65536.)

        Raises:
            ValueError: Ball position coordinates must be in range [0., 65536.); only 255 balls allowed
        """
        
        # check boundaries for ball count
        upper_boundary = 2**(8*self._BALL_COUNT_BYTES)-1
        if len(ball_positions) < 1 or len(ball_positions) > upper_boundary:
            raise ValueError("Argument \"ball_positions\" must have [%d, %d] elements (currently: %d)!" % 
                             (1, upper_boundary, len(ball_positions)))
    
        # check boundaries for positions argument
        upper_boundary = 2**(8*self._BALL_POSITION_BYTES)
        for ball_coordinates in ball_positions:
            if any(coordinate < 0. or coordinate >= upper_boundary for coordinate in ball_coordinates):
                raise ValueError("Argument \"ball_position\" must be in range [0., %f) (currently: (%f, %f))!" 
                                 % (upper_boundary, ball_coordinates[0], ball_coordinates[1]))
        
        self.__ball_positions = ball_positions
        
    @property
    def player_angles(self) -> List[float]:
        """
        Getter-method for attribute \"player_angles\".

        Returns:
            List[float]: Angles of players' platforms [0., 1.]
        """
        
        return self.__player_angles
    
    @player_angles.setter
    def player_angles(self, player_angles: List[float]) -> None:
        """
        Setter-method for attribute \"player_angles\".

        Args:
            player_angles (List[float]): Angles of players' platforms [0., 1.]

        Raises:
            ValueError: Platform angles must be in range [0., 1.]; only 255 players allowed
        """
        
        # check boundaries for player count
        upper_boundary = 2**(8*self._PLAYER_COUNT_BYTES)-1
        if len(player_angles) < 1 or len(player_angles) > upper_boundary:
            raise ValueError("Argument \"player_angles\" must have [%d, %d] elements (currently: %d)!" % 
                             (1, upper_boundary, len(player_angles)))
    
        # check boundaries for angles argument
        for player_angle in player_angles:
            if player_angle < 0. or player_angle > 1.:
                raise ValueError("Argument \"player_angle\" must be in range [0., 1.] (currently: %f)!" % player_angle)
        
        self.__player_angles = player_angles
        
    @property
    def player_lives(self) -> List[int]:
        """
        Getter-method for attribute \"player_lives\".

        Returns:
            List[int]: Players' number of remaining lives [0, 255]
        """
        
        return self.__player_lives
    
    @player_lives.setter
    def player_lives(self, player_lives: List[int]) -> None:
        """
        Setter-method for attribute \"player_lives\".

        Args:
            player_lives (List[int]): Players' number of remaining lives [0, 255]

        Raises:
            ValueError: Lives count must be in range [0, 255]; only 255 players allowed
        """
        
        # check boundaries for player count
        upper_boundary = 2**(8*self._PLAYER_COUNT_BYTES)-1
        if len(player_lives) < 1 or len(player_lives) > upper_boundary:
            raise ValueError("Argument \"player_angles\" must have [%d, %d] elements (currently: %d)!" % 
                             (1, upper_boundary, len(player_lives)))
    
        # check boundaries for lives argument
        upper_boundary = 2**(8*self._PLAYER_LIVES_BYTES)-1
        for player_lives_count in player_lives:
            if player_lives_count < 0 or player_lives_count > upper_boundary:
                raise ValueError("Argument \"player_lives_count\" must be in range [0, %d] (currently: %d)!" 
                                 % (upper_boundary, player_lives_count))
        
        self.__player_lives = player_lives
        
    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.
        
        Raises:
            ValueError: Number of player angles must equal number of player lives

        Returns:
            bytes: Serialized data
        """
        
        # check if number of player angles equals number of player lives
        player_count = len(self.__player_angles)
        player_lives_count = len(self.__player_lives)
        
        if player_count != player_lives_count:
            raise ValueError("Number of elements in \"player_angles\" (currently: %d) must equal number of elements in \
                             \"player_lives\" (currently: %d)!"
                             % (player_count, player_lives_count))
        
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        player_count_bytes = struct.pack("B", player_count)
        
        # convert 8 byte floating point datatypes to integer representations of 2 byte fixed point datatype, where 
        # 15 bits are used for the fractional part and 1 bit is used for the integer part (since angles are in range 
        # [0., 1.])
        player_angles_int = [int(player_angle * (2**self._PLAYER_ANGLE_FRAC_BITS)) 
                             for player_angle in self.__player_angles]
        # player_angles_int will always be in range [0, 2^15]!
        
        # for each player angle, cast python integer type to ctypes 2 byte unsigned integer type (abbr. "H") and get 
        # byte-wise data representation
        player_angles_bytes = struct.pack("%dH" % player_count, *player_angles_int)
        
        # for each player lives count, cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and 
        # get byte-wise data representation
        player_lives_bytes = struct.pack("%dB" % player_count, *self.__player_lives)
        
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        ball_count_bytes = struct.pack("B", len(self.__ball_positions))
        
        # cast 8 byte floating point datatypes to python integer datatypes, truncating decimal places
        ball_coordinates_int = [int(ball_coordinate) for ball_coordinates in self.__ball_positions 
                                for ball_coordinate in ball_coordinates]
        # for each ball coordinate, cast python integer type to ctypes 2 byte unsigned integer type (abbr. "H") and get 
        # byte-wise data representation
        ball_coordinates_bytes = struct.pack("%dH" % len(ball_coordinates_int), *ball_coordinates_int)
        
        # concatenate all bytes objects, accordning to the transmission protocol
        return player_count_bytes + player_angles_bytes + player_lives_bytes + ball_count_bytes + ball_coordinates_bytes
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'GameStateMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
        
        Raises:
            ValueError: Data must match expectations, according to the transmission protocol 
            
        Returns:
            GameStateMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data does not deceed/exceed expectations according to the transmission protocol
        # min. 1 player, min. 0 balls
        lower_boundary = (cls._PLAYER_COUNT_BYTES + cls._PLAYER_ANGLE_BYTES + cls._PLAYER_LIVES_BYTES + 
                          cls._BALL_COUNT_BYTES)
        # max. 255 players, max. 255 balls
        upper_boundary = (cls._PLAYER_COUNT_BYTES + (cls._PLAYER_ANGLE_BYTES + cls._PLAYER_LIVES_BYTES) * 
                          (2**(8*cls._PLAYER_COUNT_BYTES) - 1) + cls._BALL_COUNT_BYTES + 2 * 
                          cls._BALL_POSITION_BYTES * ((2**(8*cls._BALL_COUNT_BYTES) - 1)))
        if len(serialized_data) < lower_boundary or len(serialized_data) > upper_boundary:
            raise ValueError("Argument \"serialized_data\" must be [%d, %d] bytes (currently: %d)!" 
                             % (lower_boundary, upper_boundary, len(serialized_data)))
            
        # according to the transmission protocol, player count is the first and player_angles, player_lives are the 
        # subsequent bytes objects
        # bytes are implicitely casted to python integers
        # check validity of player count (must be in range [1, 255] - max. 255 is already implecitely restricted by 1 
        # byte unsigned byte type)
        start_idx = 0
        player_count = serialized_data[start_idx]
        if player_count < 1:
            raise ValueError("Transmitted value of \"player_count\" (currently: %d) is out of range [%d, %d]!" % 
                             (player_count, 1, 2**(8*cls._PLAYER_COUNT_BYTES) - 1))
        # there is no possibility to check, if player count matches number of remaining player bytes, since it's unknown
        # where player bytes end
        # get player_angles_bytes
        start_idx += 1
        player_angles_bytes_count = player_count * cls._PLAYER_ANGLE_BYTES
        player_angles_bytes = serialized_data[start_idx:start_idx+player_angles_bytes_count]
        # get ctypes 2 byte unsigned integer (abbr. "H") from byte-wise data representation (automatically cast to 
        # python integer type)
        player_angles_int = list(struct.unpack("%dH" % player_count, player_angles_bytes))
        fractional_div = 2**cls._PLAYER_ANGLE_FRAC_BITS
        player_angles = [player_angle / fractional_div for player_angle in player_angles_int]
        # get player_lives_bytes
        start_idx += player_angles_bytes_count
        player_lives_bytes_count = player_count * cls._PLAYER_LIVES_BYTES
        player_lives_bytes = serialized_data[start_idx:start_idx+player_lives_bytes_count]
        # get ctypes 1 byte unsigned integer (abbr. "B") from byte-wise data representation (automatically cast to 
        # python integer type)
        player_lives = list(struct.unpack("%dB" % player_count, player_lives_bytes))
        
        # according to the transmission protocol, ball count is the next and ball_coordinates is the subsequent bytes 
        # object
        start_idx += player_lives_bytes_count
        ball_count = serialized_data[start_idx]
        # compute expected number of remaining bytes
        start_idx += 1
        ball_coordinates_bytes = serialized_data[start_idx:]
        ball_positions_count = math.ceil(len(ball_coordinates_bytes) / (2 * cls._BALL_POSITION_BYTES))
        # check if expected number of balls matches the number of transmitted ball positions
        if ball_positions_count != ball_count:
            raise ValueError("Transmitted value of \"ball_count\" (currently: %d) doesn't match number of transmitted\
                             ball_positions (currently: %d)! Player data might be incorrect as well." 
                             % (ball_count, ball_positions_count))
        # get ctypes 2 byte unsigned integer (abbr. "H") from byte-wise data representation (automatically cast to 
        # python integer type)
        ball_coordinates_int = list(struct.unpack("%dH" % 2*ball_count, ball_coordinates_bytes))
        # cast coordinates from integer to float
        ball_coordinates = [float(ball_coordinate) for ball_coordinate in ball_coordinates_int]
        # create tuples of subsequent coordinates -> represent ball positions
        ball_positions = list(zip(*[iter(ball_coordinates)]*2))
        
        return GameStateMessage(ball_positions, player_angles, player_lives)