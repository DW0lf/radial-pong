#!/usr/bin/env python3

import struct
from typing import Union

from shared.serialization.serializable_message_type import SerializableMessageType


class PlayerCountMessage(SerializableMessageType):
    """Implements message being sent from server to client to inform about a changed number of participating players."""
    
    # identifier for message type, according to the transmission protocol
    _MESSAGE_TYPE_ID = 9
    
    def __init__(self, player_count: int) -> None:
        """
        Initializes GameStartMessage.

        Args:
            player_count (int): Number of participating players
        """
        
        self.player_count = player_count
        
    @property
    def player_count(self) -> int:
        """
        Getter-method for attribute \"player_count\".

        Returns:
            int: Players' Number of participating players
        """
        
        return self.__player_count
    
    @player_count.setter
    def player_count(self, player_count: int) -> None:
        """
        Setter-method for attribute \"player_count\".

        Args:
            player_count (int): Number of participating players

        Raises:
            ValueError: Number of players must be in range [1, 255]; only 255 ids allowed
        """
        
        # check boundaries for player count
        upper_boundary = 2**(8*self._PLAYER_COUNT_BYTES)-1
        if player_count < 1 or player_count > upper_boundary:
            raise ValueError("Argument \"player_count\" must be in range [%d, %d] (currently: %d)!" % 
                             (1, upper_boundary, player_count))
        
        self.__player_count = player_count

    def serialize(self) -> bytes:
        """
        Serializes encapsulated data.

        Returns:
            bytes: Serialized data
        """
        
        # cast python integer type to ctypes 1 byte unsigned integer type (abbr. "B") and get byte-wise data 
        # representation
        player_count_bytes = struct.pack("B", self.__player_count)
        
        # return bytes object, according to the transmission protocol
        return player_count_bytes
    
    @classmethod
    def deserialize(cls, serialized_data: bytes, player_id: Union[int, None] = None) -> 'PlayerCountMessage':
        """
        Deserializes bytes and encapsulates actual data.
        
        Args:
            serialized_data (bytes): Serialized data
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must not deceed/exceed expectations, according to the transmission 
            protocol (1 byte); transmitted value of player_count must be valid (in range [1, 255])
            
        Returns:
            PlayerCountMessage: Deserialized data, being encapsulated in class instance
        """
        
        # check, if byte count of given data does not deceed/exceed expectations according to the transmission protocol
        if len(serialized_data) != cls._PLAYER_COUNT_BYTES:
            raise ValueError("Argument \"serialized_data\" must be %d bytes (currently: %d)!" % 
                             (cls._PLAYER_COUNT_BYTES, len(serialized_data)))
        
        # according to the transmission protocol, player count is the first byte
        # bytes are implicitely casted to python integers
        # check validity of player count (must be in range [1, 255] - max. 255 is already implicitely restricted by 1 
        # byte unsigned byte type)
        if serialized_data[0] < 1:
            raise ValueError("Transmitted value of \"player_count\" (currently: %d) is out of range [%d, %d]!" % 
                             (serialized_data[0], 1, 2**(8*cls._PLAYER_COUNT_BYTES) - 1))
            
        player_count = serialized_data[0]
        
        # return new class instance, which encapsulates the deserialized data
        return PlayerCountMessage(player_count)