#!/usr/bin/env python3

from shared.serialization.is_alive_message import IsAliveMessage
import struct
from typing import Dict, Union

from shared.serialization.serializable_message_type import SerializableMessageType
from shared.serialization.connection_request_message import ConnectionRequestMessage
from shared.serialization.connection_confirmation_message import ConnectionConfirmationMessage
from shared.serialization.player_count_message import PlayerCountMessage
from shared.serialization.game_start_request_message import GameStartRequestMessage
from shared.serialization.game_start_message import GameStartMessage
from shared.serialization.game_stop_message import GameStopMessage
from shared.serialization.game_state_message import GameStateMessage
from shared.serialization.client_input_message import ClientInputMessage
from shared.serialization.client_disconnect_message import ClientDisconnectMessage

    
class BaseSerializer:
    """Implements interface for serializer instances."""
    
    # mapping from message type ids to message type classes, according to the transmission protocol
    _MAP_ID_MESSAGE_TYPE = {
        1: ConnectionRequestMessage,
        2: ConnectionConfirmationMessage,
        3: GameStartRequestMessage,
        4: GameStartMessage,
        5: GameStopMessage,
        6: GameStateMessage,
        7: ClientInputMessage,
        8: ClientDisconnectMessage,
        9: PlayerCountMessage,
        10: IsAliveMessage
    }
    
    def __init__(self) -> None:
        """
        Initializes Serializer.

        Raises:
            NotImplementedError: Must be overridden for concrete serialization classes
        """
        
        if type(self) is BaseSerializer:
            raise NotImplementedError
        else:
            self.__network_handler = None
            self.__game = None
    
    @classmethod
    @property
    def MAP_ID_MESSAGE_TYPE(cls) -> Dict[int, SerializableMessageType]:
        """
        Getter-method for read-only class attribute \"MAP_ID_MESSAGE_TYPE\".

        Raises:
            NotImplementedError: Abstract class \"Serializer\" is not a concrete, hence valid serialization class

        Returns:
            Dict[int, SerializableMessageType]: Mapping from message type ids to message type classes, according to the 
            transmission protocol
        """
        
        if cls is BaseSerializer:
            raise NotImplementedError
        else:
            return cls._MAP_ID_MESSAGE_TYPE
    
    @staticmethod
    def _get_content_bytes(message: SerializableMessageType) -> bytes:
        """
        Serializes data according to the given message type.

        Args:
            message (SerializableMessageType): Message to be serialized
            
        Returns:
            bytes: Content data (serialized message)
        """
        
        # get serialized message data
        message_bytes = message.serialize()
        
        # add the message type id as a preceeding byte (abbr. "B")
        message_type_id_byte = struct.pack("B", message.MESSAGE_TYPE_ID)
        return message_type_id_byte + message_bytes
    
    @classmethod
    def _get_message(cls, content_bytes: bytes, player_id: Union[int, None] = None) -> SerializableMessageType:
        """
        Deserializes data according to the included message type.

        Args:
            content_bytes (bytes): Data to be deserialized
            player_id (int, optional): Player's identifier [0, 255]
            
        Raises:
            ValueError: Byte count of given data must match expectations, according to the transmission protocol 
            (min. 1 byte); transmitted value of message_type_id must be valid

        Returns:
            SerializableMessageType: Message including deserialized data
        """
        
        # check, if byte count of given data matches expectations according to the transmission protocol
        # length of message data is unknown, so total length must be at least one byte
        if len(content_bytes) < 1:
            raise ValueError("Argument \"content_data\" must be at least one byte (currently: %d)!" 
                             % len(content_bytes))
        
        # according to the transmission protocol, message_type_id is the first byte and message_data are the remaining 
        # bytes
        # bytes are implicitely casted to python integers
        message_type_id = content_bytes[0]
        message_data = content_bytes[1:]
        # check, if message_type_id is valid according to the transmission protocol
        if message_type_id not in cls._MAP_ID_MESSAGE_TYPE.keys():
            raise ValueError("Transmitted value of \"message_type_id\" (currently: %d) is invalid!" % 
                             message_type_id)
        
        # instantiate new message, deserializing the given message data
        return cls._MAP_ID_MESSAGE_TYPE[message_type_id].deserialize(message_data, player_id)