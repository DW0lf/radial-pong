#!/usr/bin/env python3

"""Script contains math utility functions."""

from typing import List
import random
import math

def get_random_direction_vector() -> List[float]:
    """Generates a random 2D vector with the length 1.

    Args:

    Returns:
        List[float]: random vector
    """
    angle = random.random() * 2 * math.pi
    x = math.cos(angle)
    y = math.sin(angle)
    return [x, y]


def check_collision_of_circles(position_a: List[float], radius_a: float, position_b: List[float], radius_b: float) -> bool:
    """Check if two circles overlap.
    
    Note:
        Compares the square of the distances for better performance.

    Args:
        position_a (List[float]): Center position of the first circle.
        radius_a (float): Radius of the first circle.
        position_b (List[float]): Center position of the second circle.
        radius_b (float): Radius of the second circle.

    Returns:
        bool: Circles overlap
    """
    square_distance_center_to_center = (position_a[0] - position_b[0]) ** 2 + (position_a[1] - position_b[1]) ** 2
    return square_distance_center_to_center <= (radius_a + radius_b) ** 2

def check_direction_of_collision(start_position: List[float], direction_vector: List[float], collision_center: List[float]):
    """Checks if direction_vector points towards the collision_center.
    The angle difference should be smaller than 90 degrees.
    
    Args:
        start_position (List[float]): Position of the moving object
        direction_vector (List[float]): Direction of the movement
        collision_center (List[float]): Center of obstacle object
    
    Returns:
        bool: The direction_vector points towards the collision_center
    """
    d_x = collision_center[0] - start_position[0]
    d_y = collision_center[1] - start_position[1]
    dot_product = d_x * direction_vector[0] + d_y * direction_vector[1]
    return dot_product >= 0


def get_cartesian_coordinate(center: List[float], radius: float, angle: float) -> List[float]:
    """Converts a polar coordinate to cartesian coordinate.

    Args:
        center (List[float]): Center of the polar coordinate system
        radius (float): Radius of the polar coordinate
        angle (float): Angle of the polar coordinate

    Returns:
        List[float]: Cartesian coordinate
    """
    x = radius * math.cos(angle) + center[0]
    y = radius * math.sin(angle) + center[1]
    return [x, y]


def get_polar_coordinate(center: List[float], position: List[float]):
    """Converts a cartesian coordinate to polar coordinate.

    Args:
        center (List[float]): Center of the polar coordinate system
        position (List[float]): Position in cartesian coordinates

    Returns:
        angle (float), radius (float): Poolar coordinate
    """
    x = position[0] - center[0]
    y = position[1] - center[1]
    angle = math.atan2(y, x)
    radius = math.sqrt(x**2 + y**2)
    return angle, radius


def get_vector_from_point_to_point(position_a: List[float], position_b: List[float]):
    """Calculates the vector from one point to a second point.

    Args:
        position_a (List[float]): Position of first point.
        position_b (List[float]): Position of second point.
    
    Returns:
        List[float]: Vector from one point to a second point
    """
    return [position_b[0] - position_a[0], position_b[1] - position_a[1]]


def normalize_vector(vector: List[float]) -> List[float]:
    """Normalizes the vector.

    Args:
        vector (List[float]): Vector to normalize

    Returns:
        List[float]: Normalized vector
    """
    length = math.sqrt(vector[0] * vector[0] + vector[1] * vector[1])
    return [vector[0] / length, vector[1] / length]


def mirror_vector(vector: List[float], normal: List[float]) -> List[float]:
    """Mirrors the vector of a surface with a given normal.
     
    Args:
        vector (List[float]): Vector to mirror.
        normal (List[float]): Normal vector of the surface.

    Returns:
        List[float]: Mirrored vector
    """
    dot_product = vector[0] * normal[0] + vector[1] * normal[1]
    x = vector[0] - 2 * dot_product * normal[0]
    y = vector[1] - 2 * dot_product * normal[1]
    return [x, y]


def get_angle_from_message_angle(number_of_players: int, index_of_player: int, message_angle: float):
    """Converts a message angle (0 to 1) to a real angle (0 to 2 pi)
    
    Args:
        number_of_player (int): Number of players in the game
        index_of_player (int): Index of the player with the angle to convert
        message_angle (float): Message angle from 0 to 1
    """
    angle_per_player = 2 * math.pi / number_of_players
    return angle_per_player * (index_of_player + message_angle)


def get_message_angle_from_angle(number_of_players: int, angle: float):
    """Converts a real angle (0 to 2 pi) to message angle (0 to 1) to 
    
    Args:
        number_of_player (int): Number of players in the game
        angle (float): Angle from 0 to 2 pi
    """
    angle_per_player = 2 * math.pi / number_of_players
    return (angle % angle_per_player) / angle_per_player
