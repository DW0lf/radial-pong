#!/usr/bin/env python3
"""File to write down all test fixtures related to game dummys."""

from shared.game.game_components import Ball, GameState
import pytest
from server.game.game import Game as ServerGame
from client.game.game import Game as ClientGame
import math

@pytest.fixture(scope="function")
def test_server_game_not_running(dummy_serializer) -> ServerGame:
    """Creates a not running game.
        
    Returns:
    Game: Created game
    """
    game = ServerGame()
    game.serializer = dummy_serializer
    game._Game__connected_players = [1, 2, 3]
    return game

@pytest.fixture(scope="function")
def test_client_game_not_connected(dummy_serializer) -> ClientGame:
    """Creates a game objekt where the client is not connected.
        
    Returns:
    Game: Created game
    """
    game = ClientGame()
    game.serializer = dummy_serializer
    return game

@pytest.fixture(scope="function")
def test_client_game_not_running(dummy_serializer) -> ClientGame:
    """Creates a not running game.
        
    Returns:
    Game: Created game
    """
    game = ClientGame()
    game.serializer = dummy_serializer
    game._Game__player_id = 42
    game._Game__connected = True

    return game

@pytest.fixture(scope="function")
def test_client_game_running_not_participating(dummy_serializer) -> ClientGame:
    """Creates a running game where the client is not participating.
        
    Returns:
    Game: Created game
    """
    game = ClientGame()
    game.serializer = dummy_serializer
    game._Game__player_id = 42
    game._Game__connected = True
    game._Game__game_is_running = True

    game_state = GameState(3)
    game_state.add_player(1)
    game_state.add_player(2)
    game_state.add_player(3)
    game_state.balls.append(Ball([18000, 30000], [1, 0]))
    game_state.balls.append(Ball([40000, 44000], [1, 0]))
    game_state.balls.append(Ball([50000, 32000], [1, 0]))
    game._Game__game_state = game_state
    return game

@pytest.fixture(scope="function")
def test_client_game_running_participating(dummy_serializer) -> ClientGame:
    """Creates a running game where the client is participating.
        
    Returns:
    Game: Created game
    """
    game = ClientGame()
    game.serializer = dummy_serializer
    game._Game__player_id = 42
    game._Game__connected = True
    game._Game__player_index = 1
    game._Game__game_is_running = True
    game._Game__in_game = True

    game_state = GameState(3)
    game_state.add_player(1)
    game_state.add_player(42)
    game_state.add_player(3)
    game_state.balls.append(Ball([18000, 30000], [1, 0]))
    game_state.balls.append(Ball([40000, 44000], [1, 0]))
    game_state.balls.append(Ball([50000, 32000], [1, 0]))
    game._Game__game_state = game_state
    return game

@pytest.fixture(scope="function")
def test_server_game_running(dummy_serializer) -> ServerGame:
    """Creates a running game.
        
    Returns:
        Game: Created game
    """
    game = ServerGame()
    game.serializer = dummy_serializer

    game_state = GameState(3)
    game_state.add_player(1)
    game_state.add_player(2)
    game_state.add_player(3)
    game._Game__game_state = game_state
    
    game._Game__game_is_running = True
    game._Game__connected_players = [1, 2, 3]

    game_state.balls.append(Ball([18000, 30000], [0, 1]))
    game_state.balls.append(Ball([40000, 44000], [1, 0]))
    angle = math.pi/4
    game_state.balls.append(Ball([50000, 32000], [math.cos(angle), math.sin(angle)]))
    
    return game