#!/usr/bin/env python3

""" Script contains tests for stop scene script from client"""

import client.gui.stop_scene as stop_scene
import pytest

class TestStopScene():
    """Tests class \"StopScene\"."""
    
    def test_create_label_text_and_color(self):
        winner_ids = []
        winner_indices = []
        winner_lives = None
        label_text, label_color = stop_scene.StopScene._StopScene__create_label_text_and_color(winner_ids, 
                                                                                               winner_indices, 
                                                                                               winner_lives)
        correct_label_text = "Game stopped."
        correct_label_color = (0, 0, 0)
        assert label_text == correct_label_text
        assert label_color == correct_label_color
        
        winner_ids = [1, 2]
        winner_indices = [0, 1]
        winner_lives = 2
        label_text, label_color = stop_scene.StopScene._StopScene__create_label_text_and_color(winner_ids, 
                                                                                               winner_indices, 
                                                                                               winner_lives)
        correct_label_text = "Game stopped."
        correct_label_color = (0, 0, 0)
        assert label_text == correct_label_text
        assert label_color == correct_label_color
        
        winner_ids = [42]
        winner_indices = [0]
        winner_lives = 3
        label_text, label_color = stop_scene.StopScene._StopScene__create_label_text_and_color(winner_ids, 
                                                                                               winner_indices, 
                                                                                               winner_lives)
        correct_label_text = "Player 42 won with 3 remaining lives!"
        correct_label_color = (255, 0, 0)
        assert label_text == correct_label_text
        assert label_color == correct_label_color