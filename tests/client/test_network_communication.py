#!/usr/bin/env python3

"""Implements tests for client's NetworkHandler."""

from tests.conftest_gui import dummy_window
from tests.conftest import STARTUP_TIME, SENDING_TIME
import pytest
from client.network.network_handler import ClientNetworkHandler, NetworkConfiguration
from client.game.game import Game as ClientGame
import struct
import time

class TestClientNetworkHandler():
    def test_failing_connection(self, free_port_localhost, dummy_serializer):
        """Tests for failing of connection if server is not initialized. A test
        for a successful connection is included in shared/test_network_communication.py"""
        config = NetworkConfiguration("127.0.0.1", free_port_localhost)

        with pytest.raises(ConnectionRefusedError) as error:
            network_handler = ClientNetworkHandler(dummy_serializer, config)
        
        # check for customized error message
        assert "Could not connect to server with address" in str(error.value)

    @pytest.mark.long_running
    @pytest.mark.parametrize('number_of_clients', [1])
    def test_long_messages_to_server(self, getServerAndClients, long_message):
        """Implements test for sending 10 messages from client to server."""
        server = getServerAndClients.getServer()
        client = getServerAndClients.getClient(0)

        for i in range(10):
            long_message[0] = i
            message = struct.pack(len(long_message)*'B', *long_message)
            client.send(message)
        while not server.serializer.data:
            time.sleep(SENDING_TIME) # for wait server to receive
        while len(server.serializer.data[0]) < 10:
            time.sleep(SENDING_TIME)
        
        for i in range(10):
            assert server.serializer.data and server.serializer.data[0], "No message received for client 1"
            message = list(struct.unpack(len(long_message)*'B', server.serializer.data[0][i]))
            assert message[0] == i 
            assert len(message) == len(long_message)
        
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [1])
    def test_ping_thread(self, getServerAndClients, test_client_game_not_running, dummy_window):
        """Test behaviour of pinging thread. Wait_for_other_players starts pining."""
        server = getServerAndClients.getServer()
        client = getServerAndClients.getClient(0)

        game = test_client_game_not_running
        game.window = dummy_window

        # starts ping
        game.wait_for_other_players()

        assert game._Game__ping_thread.is_alive() == True, "Ping thread not initialized"
        assert game.connected == True, "Client not connected to server"
        server.close_connection()

        game._Game__ping_thread.join()

        assert game.connected == False, "Client still connected to server"
        assert game._Game__ping_thread.is_alive() == False, "Ping thread not closed"

        getServerAndClients.close()
