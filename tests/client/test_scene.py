#!/usr/bin/env python3

""" Script contains tests for scene scripts from client"""

from client.gui.scene_factory import create_scene
from client.gui.scene_information import SceneType
import pytest


class TestScene():
    """Test Scene object behaviour."""
    def test_start_scene(self, start_scene):
        assert start_scene.scene_type == SceneType.START_SCENE
    
    def test_waiting_scene(self, waiting_scene):
        assert waiting_scene.scene_type == SceneType.WAITING_SCENE
    
    def test_game_scene(self, game_scene):
        assert game_scene.scene_type == SceneType.GAME_SCENE

    def test_stop_scene(self, stop_scene):
        assert stop_scene.scene_type== SceneType.STOP_SCENE

    def test_update(self, start_scene, dummy_scene_information):
        start_scene.update(dummy_scene_information.start_scene_information())
        assert start_scene.scene_type == SceneType.START_SCENE

        with pytest.raises(TypeError) as se:
            start_scene.update(dummy_scene_information.stop_scene_information())

        assert "Given scene information is not of type" in str(se.value)

    def test_create_scene(self, dummy_scene_information, dummy_window, dummy_game):
        """Test for the create_scene function"""
        start_scene = create_scene(dummy_scene_information.start_scene_information(), dummy_window, dummy_game, None)
        waiting_scene = create_scene(dummy_scene_information.waiting_scene_information(), dummy_window, dummy_game, None)
        game_scene = create_scene(dummy_scene_information.game_scene_information(), dummy_window, dummy_game, None)
        stop_scene = create_scene(dummy_scene_information.stop_scene_information(), dummy_window, dummy_game, None)

        assert start_scene.scene_type == SceneType.START_SCENE
        assert waiting_scene.scene_type == SceneType.WAITING_SCENE
        assert game_scene.scene_type == SceneType.GAME_SCENE
        assert stop_scene.scene_type == SceneType.STOP_SCENE
