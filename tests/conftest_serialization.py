#!/usr/bin/env python3

import pytest
from shared.serialization.serializable_message_type import SerializableMessageType


class DummySerializer():
    def __init__(self):
        """Initialize dummy Serializer for tests."""
        self.data = list()

    def deserialize(self, message, player_id):
        while player_id > len(self.data)-1:
            self.data.append(list())
        self.data[player_id].append(message)
    
    def serialize_for_one(self, message: SerializableMessageType, player_id: int):
        return

    def serialize_for_all(self, message: SerializableMessageType):
        return

    def serialize(self, message: SerializableMessageType):
        return
    
    def print_statistics(self):
        return


@pytest.fixture(scope="function")
def dummy_serializer():
    return DummySerializer()