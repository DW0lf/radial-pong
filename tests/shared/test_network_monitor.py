#!/usr/bin/env python3

"""Implements tests for the listener on the server."""

import time
from tests.conftest import SENDING_TIME
from server.network.network_statistics import NetworkMonitor
from shared.network.connection_handler import BaseConnectionHandler, ENDIANESS
import pytest
from time import sleep

class TestNetworkMonitor:
    @pytest.mark.parametrize('number_of_clients', [2])
    def test_package_received_counter(self, getServerAndClients, example_message2):
        """Tests network monitor's received message counter behaviour with two clients."""
        # start server and monitor
        local_server = getServerAndClients.getServer()
        monitor = local_server.monitor

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)
        
        # serialize message
        serialized_message = (example_message2).to_bytes(2,ENDIANESS)

        # send messages from client 0 to server
        counter_0 = 0
        for i in range(3):
            local_client.send(serialized_message)
            counter_0 += 1
        
        for _ in range(50):
            if monitor.all_recv_packages() == counter_0:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.all_recv_packages() == counter_0
        assert monitor.received_packages_client(0) == counter_0
        
        # send messages from client 1 to server
        counter_1 = 0
        for i in range(2):
            local_client2.send(serialized_message)
            counter_1 += 1

        for _ in range(50):
            if monitor.all_recv_packages() == counter_0 + counter_1:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.all_recv_packages() == counter_0 + counter_1
        assert monitor.received_packages_client(0) == counter_0
        assert monitor.received_packages_client(1) == counter_1
        
        # send messages from client 0 and client 1 in alternating mode to server
        for i in range(2):
            local_client.send(serialized_message)
            counter_0 += 1
            local_client2.send(serialized_message)
            counter_1 += 1
        
        for _ in range(50):
            if monitor.all_recv_packages() == counter_0 + counter_1:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.all_recv_packages() == counter_0 + counter_1
        assert monitor.received_packages_client(0) == counter_0
        assert monitor.received_packages_client(1) == counter_1

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_package_sent_counter(self, getServerAndClients, example_message2):
        """Tests network monitor's sent message counter behaviour with two clients """
        # start server
        local_server = getServerAndClients.getServer()
        monitor = local_server.monitor

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)
        
        # serialize message
        serialized_message = (example_message2).to_bytes(2,ENDIANESS)

        # send messages from server to client 0
        counter_0 = 0
        for i in range(3):
            counter_0 += 1
            local_server.send(serialized_message, 0)
        
        for _ in range(50):
            if monitor.all_sent_packages() == counter_0:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.all_sent_packages() == counter_0 
        assert monitor.sent_packages_client(0) == counter_0
        assert monitor.sent_packages_client(1) == 0
        
        # send messages from server to client 1
        counter_1 = 0
        for i in range(3):
            counter_1 += 1
            local_server.send(serialized_message, 1)
        
        for _ in range(50):
            if monitor.all_sent_packages() == counter_0 + counter_1:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.all_sent_packages() == counter_0 + counter_1
        assert monitor.sent_packages_client(0) == counter_0
        assert monitor.sent_packages_client(1) == counter_1
        
        # send messages from server to all clients
        for i in range(2):
            counter_0 += 1
            counter_1 += 1
            local_server.send_to_all(serialized_message)
        
        for _ in range(50):
            if monitor.all_sent_packages() == counter_0 + counter_1:
                break
            time.sleep(SENDING_TIME)

        assert monitor.all_sent_packages() == counter_0 + counter_1
        assert monitor.sent_packages_client(0) == counter_0
        assert monitor.sent_packages_client(1) == counter_1

        getServerAndClients.close()
    

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_bytes_sent_counter(self, getServerAndClients, example_message2):
        """Tests network monitor's sent bytes counter behaviour with two clients """
        # setup monitor
        monitor = NetworkMonitor()
        # start server
        local_server = getServerAndClients.getServer()
        monitor = local_server.monitor

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        # send messages from server to client 0
        sum_sent_bytes_1 = 0
        for i in range(2, 5):
            serialized_message = (example_message2).to_bytes(i, ENDIANESS)
            sum_sent_bytes_1 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_server.send(serialized_message, 0)
        
        for _ in range(50):
            if monitor.sent_bytes_client(0) == sum_sent_bytes_1:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.sent_bytes_client(0) == sum_sent_bytes_1
        assert monitor.sent_bytes_client(1) == 0
        
        # send messages from server to all clients
        bytes_count = 2
        sum_sent_bytes_2 = 0
        for i in range(3):
            serialized_message = (example_message2).to_bytes(bytes_count, ENDIANESS)
            sum_sent_bytes_2 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_server.send_to_all(serialized_message)
        
        for _ in range(50):
            if monitor.sent_bytes_client(0) == sum_sent_bytes_1 + sum_sent_bytes_2 and monitor.sent_bytes_client(1) ==  sum_sent_bytes_2:
                break
            time.sleep(SENDING_TIME)

        assert monitor.sent_bytes_client(0) ==  sum_sent_bytes_1 + sum_sent_bytes_2
        assert monitor.sent_bytes_client(1) ==  sum_sent_bytes_2

        getServerAndClients.close()
        
    @pytest.mark.parametrize('number_of_clients', [2])
    def test_bytes_receive_counter(self, getServerAndClients, example_message2):
        """Tests network monitor's recv bytes counter behaviour with two clients """
        # setup monitor
        monitor = NetworkMonitor()
        # start server
        local_server = getServerAndClients.getServer()
        monitor = local_server.monitor

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        # send messages from client 0 to server
        sum_recv_bytes_1 = 0
        for i in range(2, 5):
            serialized_message = (example_message2).to_bytes(i, ENDIANESS)
            sum_recv_bytes_1 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_client.send(serialized_message)
        
        for _ in range(50):
            if monitor.received_bytes_client(1) == 0:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.received_bytes_client(1) == 0
        
        # send messages from client 1 to server 
        bytes_count = 2
        sum_recv_bytes_2 = 0
        for i in range(3):
            serialized_message = (example_message2).to_bytes(bytes_count, ENDIANESS)
            sum_recv_bytes_2 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_client2.send(serialized_message)
        
        for _ in range(50):
            if monitor.received_bytes_client(0) == sum_recv_bytes_1 and monitor.received_bytes_client(1) == sum_recv_bytes_2:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.received_bytes_client(0) ==  sum_recv_bytes_1
        assert monitor.received_bytes_client(1) ==  sum_recv_bytes_2
        
        # send messages from clients to server in alternating mode
        bytes_count = 2
        for i in range(3):
            serialized_message = (example_message2).to_bytes(i+bytes_count, ENDIANESS)
            sum_recv_bytes_1 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_client.send(serialized_message)
            serialized_message = (example_message2).to_bytes(i*2+bytes_count, ENDIANESS)
            sum_recv_bytes_2 += len(serialized_message) + BaseConnectionHandler.PACKAGE_HEADER_LENGTH
            local_client2.send(serialized_message)
        
        for _ in range(50):
            if monitor.received_bytes_client(0) == sum_recv_bytes_1 and monitor.received_bytes_client(1) == sum_recv_bytes_2:
                break
            time.sleep(SENDING_TIME)
        
        assert monitor.received_bytes_client(0) ==  sum_recv_bytes_1
        assert monitor.received_bytes_client(1) ==  sum_recv_bytes_2
        
        getServerAndClients.close()
