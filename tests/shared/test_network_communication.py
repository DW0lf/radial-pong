#!/usr/bin/env python3

"""Implements tests for the connection of server to client(s)."""

from tests.conftest import SENDING_TIME, STARTUP_TIME
import pytest
import time
import struct
from shared.network.connection_handler import BaseConnectionHandler, ENDIANESS

class TestNetworking:
    @pytest.mark.parametrize('number_of_clients', [1])
    def test_client_connect(self, getServerAndClients):
        """Tests connection of client to server with dummy Listener and Serializer."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client 
        local_client = getServerAndClients.getClient(0)

        # determine ip address and port of server by client
        server_ip, server_port = local_client._sock.getpeername()
        assert server_ip == local_server.ip_address
        assert server_port == local_server.port

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [1])
    def test_message_to_server(self, getServerAndClients, single_message):
        """Tests sending from a client to the server (with dummy serializer and 
        listener)."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client 
        local_client = getServerAndClients.getClient(0)

        # send message from client -> server
        local_client.send((single_message).to_bytes(2,ENDIANESS))       
        while not local_server.serializer.data:
            time.sleep(SENDING_TIME)
        while len(local_server.serializer.data[0]) < 1:
            time.sleep(SENDING_TIME)
        assert local_server.serializer.data , "No message received"
        message_client = local_server.serializer.data[0][0]
        assert int.from_bytes(message_client, ENDIANESS) == single_message, "Received message does not match sent message"

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_server_get_client_state(self, getServerAndClients):
        """Tests server NetworkHandler's (with dummy serializer and listener) 
        method get_client_state for two connected clients."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients
        local_client = getServerAndClients.getClient(0)

        local_client2 = getServerAndClients.getClient(1)

        assert local_server.get_client_state(0) == True
        assert local_server.get_client_state(1) == True
        # the test for an inactive client is included in test_server_remove_player() 
        
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_server_remove_player(self, getServerAndClients, single_message):
        """Tests ServerNetworkHandler's (with dummy serializer and listener) 
        method remove_player(), which removes a client after its sending queue 
        on the server overflows (more than 10 elements)."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        # close connection of client 1
        local_client.close_connection()

        for _ in range(0, 11):
            local_server.send((single_message).to_bytes(2,ENDIANESS), 0)
        
        time.sleep(3) # wait for finished sending process
        assert local_server.get_client_state(0) == False
        assert local_server.get_client_state(1) == True
        
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_server_get_client_port(self, getServerAndClients):
        """Tests server NetworkHandler's (with dummy serializer and 
        listener) method get_client_port for two connected clients."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        assert local_server.get_client_port(0) == local_client._sock.getsockname()[1]
        assert local_server.get_client_port(1) == local_client2._sock.getsockname()[1]
        
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_server_get_client_ip(self, getServerAndClients):
        """Tests server NetworkHandler's (with dummy serializer and 
        listener) get_client_ip method for two connected clients."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        assert local_server.get_client_ip(0) == local_client._sock.getsockname()[0]
        assert local_server.get_client_ip(1) == local_client2._sock.getsockname()[0]
        
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [1])
    def test_remove_player_crc(self, getServerAndClients, single_message):
        """Tests removing of player by server if it received 10 messages for which 
        CRC failed (failing CRC is simulated by increasing network monitors error 
        counter)."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client 
        local_client = getServerAndClients.getClient(0)
        
        # check if client connected sucessfully
        assert local_server.get_client_state(0) == True, "Initial connection of client to server failed"

        # simulated 9 failing messages
        for _ in range(9):
            local_server.monitor.increment_err_counter(0)
        
        # send a single message
        local_client.send((single_message).to_bytes(2,ENDIANESS))
        
        time.sleep(2 * STARTUP_TIME)

        # check if client is still connected
        assert local_server.get_client_state(0) == True, "Client was removed after CRC failed 9 times (maybe sending of the 10th messages failed)"
        
        # simulated 10 failing messages
        for _ in range(10):
            local_server.monitor.increment_err_counter(0)

        # wait for removing of player
        time.sleep(2 * STARTUP_TIME)

        # check if connection to client was closed
        assert local_server.get_client_state(0) == False, "Client was not removed by server after CRC failed 10 times"

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_send_long_messages_alternating(self, getServerAndClients, long_message):
        """Tests sending long messages betweeen server (with dummy serializer and 
        listener) and two clients in alternating mode."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client
        local_client1 = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        long_message_1 = long_message + [1]
        long_message_2 = long_message + [2]
        
        # send messages between server and client 1
        cycle_count = 10 # number of cycles, must be even
        for idx in range(cycle_count):
            long_message[0] = idx
            server_data = struct.pack(len(long_message)*'B', *long_message)
            long_message_1[0] = idx
            client1_data = struct.pack(len(long_message_1)*'B', *long_message_1)
            local_server.send(server_data, 0)
            local_client1.send(client1_data)
            if idx % 2 == 0:
                long_message_2[0] = idx
                client2_data = struct.pack(len(long_message_2)*'B', *long_message_2)
                local_client2.send(client2_data)
            time.sleep(SENDING_TIME)
        
        # check messages on client 1
        for idx in range(cycle_count):
            while len(local_client1.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            client_data = local_client1.serializer.data[0][idx]
            client_message = list(struct.unpack(
                len(long_message)*'B', client_data))
            assert client_message[0] == idx
            assert len(client_message) == len(long_message)
        
        # check messages on server for client 1
        for idx in range(cycle_count):
            while len(local_server.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            server_data = local_server.serializer.data[0][idx]
            server_message = list(struct.unpack(
                len(long_message_1)*'B', server_data))
            assert len(server_message) == len(long_message_1)
            assert server_message[-1] == 1
            assert server_message[0] == idx
        
        # check messages on server for client 2
        for idx in range(cycle_count//2):
            while len(local_server.serializer.data[1]) < idx + 1:
                time.sleep(SENDING_TIME)
            server_data = local_server.serializer.data[1][idx]
            server_message = list(struct.unpack(
                len(long_message_2)*'B', server_data))
            assert server_message[0] == idx * 2
            assert server_message[-1] == 2
            assert len(server_message) == len(long_message_2)
            
        getServerAndClients.close()
    
    @pytest.mark.parametrize('number_of_clients', [1])
    def test_skip_lost_message(self, getServerAndClients, long_message):
        """Tests the skipping of messages, which were not received, after a 
        specific number of messages were received 
        (BaseConnectionHandler.SKIP_COUNT_LOST_MESSAGE). 
        """
        skip_count = BaseConnectionHandler.SKIP_COUNT_LOST_MESSAGE
        local_server = getServerAndClients.getServer()

        # start client
        local_client1 = getServerAndClients.getClient(0)
        
        client_socket = local_client1._sock
        
        for i in range(1, skip_count + 1):
            long_message[0] = i
            data = struct.pack(len(long_message)*'B', *long_message)
            composed_package = BaseConnectionHandler.compose(data, i)
            bytes_send = client_socket.send(composed_package)
            assert bytes_send is not None, "Client could not send data to server"
            assert bytes_send > 0, "Client could not send data to server"
        
        # check that client is connected and server has not already received something
        assert local_server.get_client_state(0) == True
        assert not local_server.serializer.data
        
        # send message that causes skipping the "lost" message
        long_message[0] = skip_count + 1
        data = struct.pack(len(long_message)*'B', *long_message)
        composed_package = BaseConnectionHandler.compose(data, i)
        _ = client_socket.send(composed_package)
        
        # wait for data to be received
        while not local_server.serializer.data:
            time.sleep(SENDING_TIME)
        
        while len(local_server.serializer.data[0]) < skip_count:
            # if the test times out here, the skipping of messages may be incorrect
            time.sleep(SENDING_TIME)
        
        # check if messages are sorted in the right order
        for idx, msg in enumerate(local_server.serializer.data[0]):
            server_message = list(struct.unpack(
                len(long_message)*'B', msg))
            assert server_message[0] == idx + 1, "Messages received in wrong order"
        
        getServerAndClients.close()
