#!/usr/bin/env python3

import pytest

from shared.serialization.game_state_message import GameStateMessage


class TestGameStateMessage():
    """Tests class \"GameStateMessage\"."""

    def test_init(self):
        message_type_id = 6
        ball_positions = [(0., 0.)]
        player_angles = [0.]
        player_lives = [1]
        game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert game_state_message.MESSAGE_TYPE_ID == message_type_id
        assert game_state_message.ball_positions == ball_positions
        assert game_state_message.player_angles == player_angles
        assert game_state_message.player_lives == player_lives
        
        ball_positions = []
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 0" in str(excinfo.value)
            
        ball_positions = [(0., 0.)]*(2**8)
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 256" in str(excinfo.value)
        
        ball_positions = [(-1., -1.)]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: (-1." in str(excinfo.value)
            
        ball_positions = [(2**16+1, 2**16+1)]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: (65537." in str(excinfo.value)
        
        ball_positions = [(0., 0.)]
        player_angles = []
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 0" in str(excinfo.value)
            
        player_angles = [0.]*(2**8)
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 256" in str(excinfo.value)
        
        player_angles = [1.5]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 1.5" in str(excinfo.value)
        
        player_angles = [-0.5]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: -0.5" in str(excinfo.value)

        player_angles = [0.]            
        player_lives = []
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 0" in str(excinfo.value)
            
        player_lives = [0.]*(2**8)
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 256" in str(excinfo.value)
                    
        player_lives = [-1]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: -1" in str(excinfo.value)
        
        player_lives = [2**8]
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        assert "currently: 256" in str(excinfo.value)
    
    def test_serialize(self):
        ball_positions = [(0., 0.)]
        player_angles = [0.]
        player_lives = [1]
        game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        serialized_data = game_state_message.serialize()
        assert serialized_data == b'\x01\x00\x00\x01\x01\x00\x00\x00\x00'
        
        ball_positions = [(1., 1.), (65535., 65535.)]
        player_angles = [0., 1.]
        player_lives = [1, 2]
        game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        serialized_data = game_state_message.serialize()
        assert serialized_data == b'\x02\x00\x00\x00\x80\x01\x02\x02\x01\x00\x01\x00\xFF\xFF\xFF\xFF'
        
        player_lives = [1, 2, 3]
        game_state_message = GameStateMessage(ball_positions, player_angles, player_lives)
        with pytest.raises(ValueError) as excinfo:
            serialized_data = game_state_message.serialize()
        assert "currently: 2" in str(excinfo.value)
        assert "currently: 3" in str(excinfo.value)
            
    def test_deserialize(self):
        serialized_data = b'\x01\x00\x00\x01\x01\x00\x00\x00\x00'
        game_state_message = GameStateMessage.deserialize(serialized_data)
        assert game_state_message.ball_positions == [(0., 0.)]
        assert game_state_message.player_angles == [0.]
        assert game_state_message.player_lives == [1]
        
        serialized_data = b'\x02\x00\x00\x00\x80\x01\x02\x02\x01\x00\x01\x00\xFF\xFF\xFF\xFF'
        game_state_message = GameStateMessage.deserialize(serialized_data)
        assert game_state_message.ball_positions == [(1., 1.), (65535., 65535.)]
        assert game_state_message.player_angles == [0., 1.]
        assert game_state_message.player_lives == [1, 2]
        
        serialized_data = b''
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
            
        serialized_data = 1788*b'\x01'
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage.deserialize(serialized_data)
        assert "currently: 1788" in str(excinfo.value)
        
        serialized_data = b'\x00\x00\x00\x01\x01\x00\x00\x00\x00'
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
            
        serialized_data = b'\x01\x00\x00\x01\x01\x00\x00\x00\x00\x00\x00'
        with pytest.raises(ValueError) as excinfo:
            game_state_message = GameStateMessage.deserialize(serialized_data)
        assert "currently: 1" in str(excinfo.value)
        assert "currently: 2" in str(excinfo.value)