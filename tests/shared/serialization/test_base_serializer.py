#!/usr/bin/env python3

import pytest

from shared.serialization.client_input_message import ClientInputMessage
from shared.serialization.client_disconnect_message import ClientDisconnectMessage
from shared.serialization.base_serializer import BaseSerializer

class TestSerializer():
    """Tests class \"BaseSerializer\"."""
    
    def test_serialize_deserialize(self):
        sent_message = ClientInputMessage(0., player_id=1)
        content_bytes = BaseSerializer._get_content_bytes(sent_message)
        recv_message = BaseSerializer._get_message(content_bytes, player_id=1)
        assert sent_message.MESSAGE_TYPE_ID == recv_message.MESSAGE_TYPE_ID
        assert sent_message.player_id == recv_message.player_id
        assert sent_message.player_angle == recv_message.player_angle
    
        sent_message = ClientDisconnectMessage()
        content_bytes = BaseSerializer._get_content_bytes(sent_message)
        recv_message = BaseSerializer._get_message(content_bytes, 0)
        assert sent_message.MESSAGE_TYPE_ID == recv_message.MESSAGE_TYPE_ID
        
        content_bytes = b''
        with pytest.raises(ValueError) as excinfo:
            recv_message = BaseSerializer._get_message(content_bytes)
        assert "currently: 0" in str(excinfo.value)
        
        content_bytes = b'\xFF'
        with pytest.raises(ValueError) as excinfo:
            recv_message = BaseSerializer._get_message(content_bytes)
        assert "currently: 255" in str(excinfo.value)