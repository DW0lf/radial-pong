#!/usr/bin/env python3

import pytest

from shared.serialization.game_start_message import GameStartMessage


class TestGameStartMessage():
    """Tests class \"GameStartMessage\"."""
    
    def test_init(self):
        message_type_id = 4
        player_ids = list(range(255))
        game_start_message = GameStartMessage(player_ids)
        assert game_start_message.MESSAGE_TYPE_ID == message_type_id
        assert game_start_message.player_ids == player_ids
        
        player_ids = []
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage(player_ids)
        assert "currently: 0" in str(excinfo.value)
            
        player_ids = list(range(256))
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage(player_ids)
        assert "currently: 256" in str(excinfo.value)
            
        player_ids = [-1]
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage(player_ids)
        assert "currently: -1" in str(excinfo.value)
            
        player_ids = [256]
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage(player_ids)
        assert "currently: 256" in str(excinfo.value)
        
    def test_serialize(self):
        player_ids = list(range(2))
        game_start_message = GameStartMessage(player_ids)
        serialized_data = game_start_message.serialize()
        assert serialized_data == b'\x02\x00\x01'
        
        player_ids = list(range(4))
        game_start_message = GameStartMessage(player_ids)
        serialized_data = game_start_message.serialize()
        assert serialized_data == b'\x04\x00\x01\x02\x03'
        
    def test_deserialize(self):
        serialized_data = b'\x02\x00\x01'
        player_ids = list(range(2))
        game_start_message = GameStartMessage.deserialize(serialized_data)
        assert game_start_message.player_ids == player_ids
        
        serialized_data = b'\x04\x00\x01\x02\x03'
        player_ids = list(range(4))
        game_start_message = GameStartMessage.deserialize(serialized_data)
        assert game_start_message.player_ids == player_ids
        
        serialized_data = 257*b'\x00'
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage.deserialize(serialized_data)
        assert "currently: 257" in str(excinfo.value)
        
        serialized_data = b'\x00\x00'
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage.deserialize(serialized_data)
        assert "currently: 0" in str(excinfo.value)
        
        serialized_data = b'\x02\x00'
        with pytest.raises(ValueError) as excinfo:
            game_start_message = GameStartMessage.deserialize(serialized_data)
        assert "currently: 2" in str(excinfo.value)
        assert "currently: 1" in str(excinfo.value)