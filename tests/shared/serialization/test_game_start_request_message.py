#!/usr/bin/env python3

import pytest

from shared.serialization.game_start_request_message import GameStartRequestMessage


class TestGameStartRequestMessage():
    """Tests class \"GameStartRequestMessage\"."""
    
    def test_init(self):
        message_type_id = 3
        game_start_request_message = GameStartRequestMessage()
        assert game_start_request_message.MESSAGE_TYPE_ID == message_type_id
            
    def test_serialize(self):
        game_start_request_message = GameStartRequestMessage()
        serialized_data = game_start_request_message.serialize()
        assert serialized_data == b''
        
    def test_deserialize(self):
        serialized_data = b''
        game_start_request_message = GameStartRequestMessage.deserialize(serialized_data)
        
        serialized_data = b'\x00'
        with pytest.raises(ValueError) as excinfo:
            game_start_request_message = GameStartRequestMessage.deserialize(serialized_data)
        assert "currently: 1" in str(excinfo.value)