#!/usr/bin/env python3

import pytest

from shared.serialization.client_input_message import ClientInputMessage


class TestClientInputMessage():
    """Tests class \"ClientInputMessage\"."""
    
    def test_init(self):
        message_type_id = 7
        player_angle = 0.
        client_input_message = ClientInputMessage(player_angle)
        assert client_input_message.player_angle == player_angle
        assert client_input_message.MESSAGE_TYPE_ID == message_type_id
        
        player_angle = 1.
        client_input_message = ClientInputMessage(player_angle)
        assert client_input_message.player_angle == player_angle
        assert client_input_message.player_id == 0
        
        player_angle = 1.5
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage(player_angle)
        assert "currently: 1.5" in str(excinfo.value)
        
        player_angle = -0.5
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage(player_angle)
        assert "currently: -0.5" in str(excinfo.value)
        
        player_angle = 1.
        player_id = -1
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage(player_angle, player_id)
        assert "currently: -1" in str(excinfo.value)
        
        player_id = 256
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage(player_angle, player_id)
        assert "currently: 256" in str(excinfo.value)
            
    def test_serialize(self):
        player_angle = 0.
        client_input_message = ClientInputMessage(player_angle)
        serialized_data = client_input_message.serialize()
        assert serialized_data == b'\x00\x00' # 0
        
        player_angle = 1.
        client_input_message = ClientInputMessage(player_angle)
        serialized_data = client_input_message.serialize()
        assert serialized_data == b'\x00\x80' # 32768
        
    def test_deserialize(self):
        serialized_data = b'\x00\x00' # 0
        client_input_message = ClientInputMessage.deserialize(serialized_data, 0)
        assert client_input_message.player_angle == 0.
        assert client_input_message.player_id == 0
        
        serialized_data = b'\x00\x80' # 32768
        client_input_message = ClientInputMessage.deserialize(serialized_data, 255)
        assert client_input_message.player_angle == 1.
        assert client_input_message.player_id == 255
        
        serialized_data = b'\x00' # 0
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage.deserialize(serialized_data, 0)
        assert "currently: 1" in str(excinfo.value)
        
        serialized_data = b'\x00\x00\x00' # 0
        with pytest.raises(ValueError) as excinfo:
            client_input_message = ClientInputMessage.deserialize(serialized_data, 0)
        assert "currently: 3" in str(excinfo.value)
        
        serialized_data = b'\x00\x00' # 0
        with pytest.raises(TypeError) as excinfo:
            client_input_message = ClientInputMessage.deserialize(serialized_data)
        assert "not be None" in str(excinfo.value)