#!/usr/bin/env python3

"""Implements tests for the listener on the server."""

import pytest
import time
from shared.network.listener import Listener
from tests.conftest import SENDING_TIME, STARTUP_TIME

class TestListener():
    @pytest.mark.parametrize('number_of_clients', [0])
    def test_init(self, getServerAndClients):
        """Test listeners initialization with dummy serializer."""
        local_server = getServerAndClients.getServer()
        listener = local_server.listener
        assert isinstance(listener, Listener)
        time.sleep(2 * STARTUP_TIME)
        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_listen(self, getServerAndClients, example_message, example_message2):
        """Unit test for Listeners method listen() with two clients. Therefore 
        the server is configured with a dummy serilizer."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients 
        local_client = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)
            
        local_client.send((example_message).to_bytes(2,'little'))
        local_client2.send((example_message2).to_bytes(2,'little'))

        dummy_serializer = local_server.serializer
        while len(local_server.serializer.data) < 2:
            time.sleep(SENDING_TIME)
        while len(local_server.serializer.data[0]) < 1 or len(local_server.serializer.data[1]) < 1:
            time.sleep(SENDING_TIME)
        assert dummy_serializer.data[0][0] == (example_message).to_bytes(2,'little')
        assert dummy_serializer.data[1][0] == (example_message2).to_bytes(2,'little')

        getServerAndClients.close()
