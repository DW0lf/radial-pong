#!/usr/bin/env python3

"""Implements tests for server's NetworkHandler."""

import socket
import struct
from tests.conftest import STARTUP_TIME, SENDING_TIME
import threading
import pytest
import time
import selectors

from shared.network.connection_handler import ENDIANESS


class TestServer:
    @pytest.mark.parametrize('number_of_clients', [0])
    def test_successful_setup(self, getServerAndClients):
        """Unit test for ServerNetworkHandler's setup() using dummy Serializer
        and Listener objects.
        """
        # start server
        local_server = getServerAndClients.getServer()

        # test if socket is active
        assert local_server.socket_active() == True
        assert local_server.listener._Listener__running, "Listener not running"

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [0])
    def test_failing_setup(self, getServerAndClients, second_free_port_localhost):
        """Unit test to check behaviour of failing setup. Therefore the
        ServerNetworkHandler must be initialized successfully first and afterwards 
        deconfigurated to fail."""
        local_server = getServerAndClients.getServer()
        # wait for server to be setup successfully
        assert local_server.socket_active() == True
        getServerAndClients.close()

        # wait for server to be shutdown successfully
        while local_server.socket_active():
            time.sleep(STARTUP_TIME)

        assert local_server.socket_active() == False

        # setup another socket to block the port
        ip = socket.gethostbyname(socket.gethostname())
        port = second_free_port_localhost
        
        # set attributes of ServerNetworkHandler to initial values
        local_server._ServerNetworkHandler__port = port
        local_server._ServerNetworkHandler__selector = selectors.DefaultSelector()
        local_server._BaseNetworkHandler__condition = threading.Condition()
        local_server._start_listener()

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            # bind the other socket
            sock.bind((ip, port))
            error = local_server._ServerNetworkHandler__setup_socket()
        
        assert not error

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [0])
    def test_close_connection(self, getServerAndClients):
        """Unit test for ServerNetworkHandler's close_connection() using dummy 
        Serializer and Listener objects.
        """
        # start server
        local_server = getServerAndClients.getServer()

        # check if server is active
        assert local_server.socket_active() == True

        # shut down server
        local_server.close_connection()
        while local_server.socket_active():
            time.sleep(STARTUP_TIME)  # wait for server to close

        assert local_server.socket_active() == False

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [0])
    def test_not_connected_player(self, getServerAndClients):
        """Unit test for ServerNetworkHandler's send method for zero connected players 
        using dummy Serializer and Listener objects.
        """
        # start server
        local_server = getServerAndClients.getServer()

        with pytest.raises(IndexError) as se:
            local_server.send((1024).to_bytes(2, ENDIANESS), 1)

        assert "There is no player with ID" in str(se.value)

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [1])
    def test_send_message_to_client(self, getServerAndClients, example_message):
        """Tests sending a message from the server (with dummy serializer and
        listener) to a single client."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client
        local_client = getServerAndClients.getClient(0)

        # send message from server -> client
        local_server.send(example_message.to_bytes(2, ENDIANESS), 0)
        while not local_client.serializer.data:
            time.sleep(SENDING_TIME)
        while len(local_client.serializer.data[0]) < 1:
            time.sleep(SENDING_TIME)

        assert local_client.serializer.data and local_client.serializer.data[
            0], "No message received for client"
        message_server_client = local_client.serializer.data[0][0]
        assert int.from_bytes(message_server_client,
                                ENDIANESS) == example_message

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_send_multiple_clients(self, getServerAndClients, example_message, example_message2):
        """Tests sending of different messages from server (with dummy serializer and 
        listener) to multiple clients."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client
        local_client = getServerAndClients.getClient(0)

        # start client
        local_client2 = getServerAndClients.getClient(1)

        local_server.send(example_message.to_bytes(2, ENDIANESS), 0)
        local_server.send(example_message2.to_bytes(2, ENDIANESS), 1) 

        while not (local_client.serializer.data and local_client2.serializer.data):
            time.sleep(SENDING_TIME)
        while len(local_client.serializer.data[0]) < 1 or len(local_client2.serializer.data[0]) < 1:
            time.sleep(SENDING_TIME)

        assert local_client.serializer.data and local_client.serializer.data[
            0], "No message received for client 1"
        assert local_client2.serializer.data and local_client2.serializer.data[
            0], "No message received for client 2"
        message_client = local_client.serializer.data[0][0]
        message_client2 = local_client2.serializer.data[0][0]
        assert int.from_bytes(message_client, ENDIANESS) == example_message
        assert int.from_bytes(message_client2, ENDIANESS) == example_message2

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_send_to_all(self, getServerAndClients, example_message):
        """Tests ServerNetworkHandler's method send_to_all (with dummy serializer and 
        listener)."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client
        local_client = getServerAndClients.getClient(0)

        # start client
        local_client2 = getServerAndClients.getClient(1)

        local_server.send_to_all(example_message.to_bytes(2, ENDIANESS))

        while not (local_client.serializer.data and local_client2.serializer.data):
            time.sleep(SENDING_TIME)
        while len(local_client.serializer.data[0]) < 1 or len(local_client2.serializer.data[0]) < 1:
            time.sleep(SENDING_TIME)

        assert local_client.serializer.data and local_client.serializer.data[
            0], "No message received for client 1"
        assert local_client2.serializer.data and local_client2.serializer.data[
            0], "No message received for client 2"
        message_client = local_client.serializer.data[0][0]
        message_client2 = local_client2.serializer.data[0][0]
        assert int.from_bytes(message_client, ENDIANESS) == example_message
        assert int.from_bytes(message_client2, ENDIANESS) == example_message

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [1])
    def test_send_long_messages_to_client(self, getServerAndClients, long_message):
        """Tests sending long messages from the server (with dummy serializer and 
        listener) to a single client."""
        # start server
        local_server = getServerAndClients.getServer()

        # start client
        local_client = getServerAndClients.getClient(0)

        # send message from server -> client, repeat 10 times
        for idx in range(50):
            long_message[0] = idx
            send_data = struct.pack(len(long_message)*'B', *long_message)
            local_server.send(send_data, 0)
            time.sleep(SENDING_TIME)
        for idx in range(50):
            while len(local_client.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            recv_data = local_client.serializer.data[0][idx]
            recv_message = list(struct.unpack(
                len(long_message)*'B', recv_data))
            assert recv_message[0] == idx
            assert len(recv_message) == len(long_message)

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_send_long_messages_to_multiple_clients(self, getServerAndClients, long_message):
        """Tests sending long messages from the server (with dummy serializer and 
        listener) to multiple client."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients
        local_client1 = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        # send message from server -> client, repeat 10 times
        for idx in range(50):
            long_message[0] = idx
            send_data = struct.pack(len(long_message)*'B', *long_message)
            local_server.send(send_data, 0)
            local_server.send(send_data, 1)
            time.sleep(SENDING_TIME)
        for idx in range(50):
            while len(local_client1.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            recv_data = local_client1.serializer.data[0][idx]
            recv_message = list(struct.unpack(
                len(long_message)*'B', recv_data))
            assert recv_message[0] == idx
            assert len(recv_message) == len(long_message)

            while len(local_client2.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            recv_data = local_client2.serializer.data[0][idx]
            recv_message = list(struct.unpack(
                len(long_message)*'B', recv_data))
            assert recv_message[0] == idx
            assert len(recv_message) == len(long_message)

        getServerAndClients.close()

    @pytest.mark.parametrize('number_of_clients', [2])
    def test_send_long_messages_to_all_clients(self, getServerAndClients, long_message):
        """Tests sending long messages from the server (with dummy serializer and 
        listener) to multiple client."""
        # start server
        local_server = getServerAndClients.getServer()

        # start clients
        local_client1 = getServerAndClients.getClient(0)
        local_client2 = getServerAndClients.getClient(1)

        # send message from server -> client, repeat 10 times
        for idx in range(50):
            long_message[0] = idx
            send_data = struct.pack(len(long_message)*'B', *long_message)
            local_server.send_to_all(send_data)
            time.sleep(SENDING_TIME)
        for idx in range(50):
            while len(local_client1.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            recv_data = local_client1.serializer.data[0][idx]
            recv_message = list(struct.unpack(
                len(long_message)*'B', recv_data))
            assert recv_message[0] == idx
            assert len(recv_message) == len(long_message)

            while len(local_client2.serializer.data[0]) < idx + 1:
                time.sleep(SENDING_TIME)
            recv_data = local_client2.serializer.data[0][idx]
            recv_message = list(struct.unpack(
                len(long_message)*'B', recv_data))
            assert recv_message[0] == idx
            assert len(recv_message) == len(long_message)
            time.sleep(SENDING_TIME)

        getServerAndClients.close()
