#!/usr/bin/env python3
"""File to write down all test fixtures."""

import sys
import os
import time
sys.path.append(os.path.join(os.getcwd(), "radialpong"))

# import all functions form other conftest files
from tests.conftest_games import *
from tests.conftest_gui import *
from tests.conftest_network import *
from tests.conftest_serialization import *
