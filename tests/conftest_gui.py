#!/usr/bin/env python3
"""File to write down all test fixtures related to gui testing."""

from shared.game.game_components import GameState, Player, PlayerPlatform
import pytest

from client.gui.game_scene import GameScene
from client.gui.start_scene import  StartScene
from client.gui.stop_scene import StopScene
from client.gui.waiting_scene import WaitingScene
from client.gui.scene_information import SceneInformation

from client.gui.scene_information import GameSceneInformation, StartSceneInformation, StopSceneInformation, WaitingSceneInformation


class DummyGame:
    
    def __init__(self) -> None:
        self.player_index = 0
        self.player_count = 1
    
    def update_player_angle(self, message_angle: float):
        pass

    def request_game_start(self):
        pass

    def start_connection(self, network_config: 'NetworkConfiguration'):
        pass

    def stop(self):
        pass

@pytest.fixture(scope="function")
def dummy_game():
    return DummyGame()

class DummySceneInformation():
    def __init__(self):
        self.game_state = GameState(3)
    
    def start_scene_information(self):
        return StartSceneInformation()

    def waiting_scene_information(self):
        return WaitingSceneInformation(1)

    def stop_scene_information(self):
        return StopSceneInformation([], [], None)

    def game_scene_information(self):
        return GameSceneInformation(self.game_state)

@pytest.fixture(scope="function")
def dummy_scene_information():
    return DummySceneInformation()

class DummyWindow():

    def start(self):
        pass

    def stop(self):
        pass

    def update_scene_information(self, scene_information: 'SceneInformation'):
        pass

@pytest.fixture(scope="function")
def dummy_window():
    return DummyWindow()

@pytest.fixture(scope="function")
def start_scene(dummy_scene_information, dummy_window, dummy_game):
    return StartScene(dummy_scene_information.start_scene_information(), dummy_window, dummy_game, None)

@pytest.fixture(scope="function")
def waiting_scene(dummy_scene_information, dummy_window, dummy_game):
    return WaitingScene(dummy_scene_information.waiting_scene_information(), dummy_window, dummy_game, None)

@pytest.fixture(scope="function")
def game_scene(dummy_scene_information, dummy_window, dummy_game):
    return GameScene(dummy_scene_information.game_scene_information(), dummy_window, dummy_game, None)

@pytest.fixture(scope="function")
def stop_scene(dummy_scene_information, dummy_window, dummy_game):
    return StopScene(dummy_scene_information.stop_scene_information(), dummy_window, dummy_game, None)
